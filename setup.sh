#!/bin/bash
sudo apt update

sudo apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository \
	   "deb [arch=amd64] https://download.docker.com/linux/debian \
	      $(lsb_release -cs) \
	         stable"
sudo apt update
sudo apt install -y docker-compose docker-ce docker-ce-cli containerd.io

sudo systemctl enable docker

sudo sysctl -w vm.max_map_count=262144
# Edit /etc/sysctl.conf to make this permanent

sudo usermod -aG docker `whoami`


sudo apt install -y python python-pip python3 python3-pip jupyter-notebook
pip install androguard elasticsearch sklearn pandas requests tld multipart
pip3 install multipart
sudo apt update
sudo apt install -y docker docker-compose

sudo docker pull docker.elastic.co/elasticsearch/elasticsearch:6.5.4
