#!/usr/bin/env python

from config import CONFIG

import predictor_utils

import pandas as pd

# Learning algorithm(s)
import sklearn
from sklearn import tree

class Tree_Predictor(predictor_utils.Predictor):
    def __init__(self):
        # A unique identifier for this predictor
        # All predictions made by this predictor
        #   are prefixed with this value
        PREDICTOR_ID = "static-receivers_tree-01"
    
        super(Tree_Predictor, self).__init__(
            predictor_id=PREDICTOR_ID,
            subscriptions=['static-receivers']
        )

    def fit(self, df, labels):
        ##############################################################################
        #       Machine Learning Block
        #           Input:  a dataframe df containing all avail data
        #           Output: a pickable model with a .predict() that is fit to df
        ##############################################################################
        # Initialize the model
        classifier = tree.DecisionTreeClassifier(random_state=0)

        # Simple model: We can just call Predictor._fit() 
        return self._fit(classifier, df, labels)
        #############################################################################


def main():
    predictor = Tree_Predictor()
    predictor_utils.driver(predictor)

# Allow direct invocation
if __name__ == "__main__":
    main()

