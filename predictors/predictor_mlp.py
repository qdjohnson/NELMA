#!/usr/bin/env python

from config import CONFIG

import predictor_utils

import pandas as pd

# Learning algorithm(s)
import sklearn
from sklearn.neural_network import MLPClassifier

class MLP_Predictor(predictor_utils.Predictor):
    def __init__(self):
        # A unique identifier for this predictor
        # All predictions made by this predictor
        #   are prefixed with this value
        PREDICTOR_ID = "static-imports_mlp-01"
    
        super(MLP_Predictor, self).__init__(
            predictor_id=PREDICTOR_ID,
            subscriptions=['static-imports']
        )

    def fit(self, df, labels):
        ##############################################################################
        #       Machine Learning Block
        #           Input:  a dataframe df containing all avail data
        #           Output: a pickable model with a .predict() that is fit to df
        ##############################################################################
        # Initialize the model
        # Note: hyperparameters likely need tuning
        classifier = MLPClassifier(random_state=0)

        # Simple model: We can just call Predictor._fit() 
        return self._fit(classifier, df, labels)
        #############################################################################


def main():
    predictor = MLP_Predictor()
    predictor_utils.driver(predictor)

# Allow direct invocation
if __name__ == "__main__":
    main()

