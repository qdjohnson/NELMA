#!/usr/bin/env python

# Probabilistic Oracle
#   Returns the ground truth according to user-specified probabilities
#   Useful for simulating uncorrelated predictors for use by the Knowledge Integrator

# Developer Note: This is a non-standard Predictor and should not be used as a template
#   for normal Predictors

from config import CONFIG

import predictor_utils

import pandas as pd

import os

# Learning algorithm(s)
import random
        

class Oracle_Predictor(predictor_utils.Predictor):
    # tpr - True Postive Rate - 0 <= tpr <= 1
    #   if the instance is True, this is the probability the oracle returns True
    # tnr - True Negative Rate - 0 <= tnr <= 1
    #   if the instance is False, this is the probability the oracle returns False
    def __init__(self, predictor_id, tpr, tnr, conf_noise):
        # A unique identifier for this predictor
        # All predictions made by this predictor
        #   are prefixed with this value
    
        if not hasattr(self, 'info'):
            self.info = {}

        print("{}:{}:{}".format(predictor_id, tpr, tnr))
        self.tpr = tpr
        self.tnr = tnr
        self.conf_noise = conf_noise

        self.info.update(
            {
            'true-positive-rate':self.tpr, 
            'true-negative-rate':self.tnr, 
            'confidence-noise':self.conf_noise, 
            }
        )
        super(Oracle_Predictor, self).__init__(
            predictor_id=predictor_id,
            subscriptions = ['samples']
        )


    def fit(self, df, labels):
        pass

    # Override Predictor.predict since we have no .classifier instance
    def get_predictions(self, df):
        return [self._predict(ground_truth) for ground_truth in df["ground_truth"]]

    # Return "noisy" confidences
    def get_confidences(self, df):
        # intruduce noise in the confidences
        confidences = [ ]
        for label in df['ground_truth']:
            prob = None
            if label:
                prob = self.tpr
            else:
                prob = self.tnr
            noise = random.uniform(-1*self.conf_noise, self.conf_noise)
            confidences.append(prob + noise)

        return confidences

    # No details
    def get_details(self, df):
        return [None]*len(df.index)

    # return boolean b with probability p
    # else, return not b
    @staticmethod
    def rand_noise(b, p):
        #print("Returning {} with prob {}".format(b, p))
        r = random.uniform(0, 1)
        if r <= p:
            return b
        else:
            return not b

    def _predict(self, ground_truth):
        if ground_truth:
            return Oracle_Predictor.rand_noise(ground_truth, self.tpr)
        else:
            return Oracle_Predictor.rand_noise(ground_truth, self.tnr)

def _main(_id):
    tpr = random.uniform(0.6, 0.8)
    tnr = random.uniform(0.6, 0.8)
    conf_noise = random.uniform(0.05, 0.15)
    predictor = Oracle_Predictor("oracle-{}".format(_id), tpr, tnr, conf_noise)
    predictor_utils.driver(predictor)
    
