#!/usr/bin/env python

from config import CONFIG
import predictor_utils

import pandas as pd

# Learning algorithm(s)
import sklearn
from sklearn.naive_bayes import GaussianNB

class NaiveBayes_Predictor(predictor_utils.Predictor):
    def __init__(self):
        # A unique identifier for this predictor
        # All predictions made by this predictor
        #   are prefixed with this value
        PREDICTOR_ID = "static-services_nb-01"
    
        super(NaiveBayes_Predictor, self).__init__(
            predictor_id=PREDICTOR_ID,
            subscriptions=['static-services']
        )

    def fit(self, df, labels):
        ##############################################################################
        #       Machine Learning Block
        #           Input:  a dataframe df containing all avail data
        #           Output: a pickable model with a .predict() that is fit to df
        ##############################################################################
        # Initialize the model
        classifier = GaussianNB()

        # Simple model: We can just call Predictor._fit() 
        return self._fit(classifier, df, labels)

        #############################################################################


def main():
    predictor = NaiveBayes_Predictor()
    predictor_utils.driver(predictor)
    
# Allow direct invocation
if __name__ == "__main__":
    main()
