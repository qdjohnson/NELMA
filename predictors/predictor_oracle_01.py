#!/usr/bin/env python

# Probabilistic Oracle
#   Returns the ground truth according to user-specified probabilities
#   Useful for simulating uncorrelated predictors for use by the Knowledge Integrator

# Developer Note: This is a non-standard Predictor and should not be used as a template
#   for normal Predictors

# This file is designed to be copied multiple times to simulate multiple predictors
# (Implementing multiple predictors in the same file conflicts with NELMA's parallelism)

from config import CONFIG

import predictor_utils

import _oracles
import os

# Learning algorithm(s)
import random


def main():
    # Turn a path like ./predictors/predictor_oracle_01.py into 01
    i = os.path.splitext(os.path.basename(__file__))[0].rsplit('_', 1)[1]
    _oracles._main(i)

# Allow direct invocation
if __name__ == "__main__":
    main()

