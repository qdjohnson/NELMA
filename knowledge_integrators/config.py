#!/usr/bin/env python
import sys, os

# add parent directory to the system path temporarily so that we can import the real config.py
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from nelma_config import *


# Note: this file is imported by the files in the same directory as this.
#   this accomplished the same thing as import ../config, but is valid syntax
#   see https://stackoverflow.com/questions/16780014/import-file-from-parent-directory/16780068
