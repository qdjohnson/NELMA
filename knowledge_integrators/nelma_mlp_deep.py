#!/usr/bin/env python

#In its simplest form, the Knowledge Integrator is a Predictor that uses
#   the output of other Predictors as its feature set.

from config import CONFIG
import predictor_utils

import pandas as pd

# Learning algorithm(s)
import sklearn
#from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier


class MLP_NELMA_KI(predictor_utils.Predictor):
    def __init__(self):
        # A unique identifier for this predictor
        # All predictions made by this predictor
        #   are prefixed with this value
        PREDICTOR_ID = "nelma-ki-mlp-deep"
        if not hasattr(self, 'info'):
            self.info = {}
        # Default settings that can be overridden by inhereting classes
        settings = {
            "ki_relabel":True,
            "relabel_threshold":0.98
        }
        for key, val in settings.items():
            # If an child class did not provide a value, use our default (defined above)
            if not key in self.info:
                self.info[key] = val

        super(MLP_NELMA_KI, self).__init__(
            predictor_id=PREDICTOR_ID,
            subscriptions=["predictions"],
            is_ki=True
        )

    def fit(self, df, labels):
        ##############################################################################
        #       Machine Learning Block
        #           Input:  a dataframe df containing all avail data
        #           Output: a pickable model with a .predict() that is fit to df
        ##############################################################################
        # Initialize the model
        classifier = MLPClassifier(hidden_layer_sizes=(30,)*3, max_iter=1000, random_state=0)
        # Simple model: We can just call Predictor._fit() 
        return self._fit(classifier, df, labels)

        #############################################################################

def main():
    predictor = MLP_NELMA_KI()
    predictor_utils.driver(predictor)

# Allow direct invocation
if __name__ == "__main__":
    main()
