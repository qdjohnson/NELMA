#!/usr/bin/env python

# Configuration file imported globally throughout NELMA
# Python is used instead of JSON / yaml to enable computed fields and unified logging

# add utils directory to the system path temporarily so they can be imported by NELMA components
import sys, os
import re

import logging
from logging.handlers import TimedRotatingFileHandler

# Expose the utils directory to everyone, so they can import it directly
sys.path.append(
  os.path.join(
    os.path.dirname( os.path.abspath(__file__) ), 
    "utils"
  )
)

CONFIG = {
  "ES_PROTO":"http",
  "ES_HOST":"localhost",
  "ES_PORT":"9200",
  "MALWARE_REGEX":re.compile("{0}amd_data{0}".format(os.path.sep)),
  "BENIGN_REGEX":re.compile("{0}benign{0}".format(os.path.sep)),
  "DEFAULT_INDEX":"static-permissions",
  "PREDICTORS_DIR":"predictors", 
  "KI_DIR":"knowledge_integrators",
  "NELMA_DIR":os.path.dirname( os.path.abspath(__file__) ),
  "TIMEOUT":10000
}
  #"PREDICTION_INDEX":"predictions",
  #"PERMISSIONS_INDEX":"static.permissions",

# Computed fields:
CONFIG["ES_URL"]="{}://{}:{}".format(
    CONFIG["ES_PROTO"], 
    CONFIG["ES_HOST"], 
    CONFIG["ES_PORT"])
# This produces a template like this: http://localhost:80/{}/1?pretty
#   calling this.format('my_index') produces a URL that can be used for HTTP API operations
CONFIG["ES_URL_TEMPLATE"] = '{}/{{}}/1?pretty'.format(CONFIG["ES_URL"])

# Compute a list of model dirs that can be deleted / created by drivers to ensure a clean start
CONFIG["MODEL_DIRS"] = [ os.path.join(CONFIG['NELMA_DIR'], CONFIG[d], 'models') for d in ['PREDICTORS_DIR', 'KI_DIR'] ]

# Set up logging
CONFIG["LOGGERS"] = {}
# https://www.toptal.com/python/in-depth-python-logging

FORMATTER = logging.Formatter("%(asctime)s - %(name)-8s - %(levelname)-8s - %(filename)s:%(funcName)s:%(lineno)d - %(message)s")
LOG_LEVEL = logging.WARNING
CONFIG['LOG_FILE'] = os.path.join(CONFIG['NELMA_DIR'], 'logs', 'nelma.log')
def add_logger(logger):
    CONFIG["LOGGERS"][name] = logger
    LOG_FILE = CONFIG['LOG_FILE']
    # Set default log level to LOG_LEVEL
    logger.setLevel(LOG_LEVEL)

    # Allow every logger to log to STDOUT
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    console_handler.propogate = False
    logger.addHandler(console_handler)
    
    # Allow every logger to log to nelma.log
    file_handler = TimedRotatingFileHandler(LOG_FILE, when='midnight')
    file_handler.setFormatter(FORMATTER)
    file_handler.propogate = False
    logger.addHandler(file_handler)
    
for name in ['NELMA', 'DB', 'PRED', 'KI', 'EXTRACT', 'DRIVER', 'DBG']:
    logger = logging.getLogger(name)
    add_logger(logger)

# Override default logging levels here based on the component you are working on
# e.g. CONFIG['LOGGERS']['DB'].setLevel(logging.DEBUG)
#CONFIG['LOGGERS']['KI'].setLevel(logging.INFO)
#CONFIG['LOGGERS']['PRED'].setLevel(logging.INFO)
CONFIG['LOGGERS']['DRIVER'].setLevel(logging.INFO)
CONFIG['LOGGERS']['EXTRACT'].setLevel(logging.INFO)

# For interactive debugging, you can use CONFIG['LOGGERS']['DBG'].info('my dbg message').
# It will be written to STDOUT, logs/nelma_log, and logs/dbg.log (by itself)
# These messages will be labeled "DBG" so you can locate them more easily
dbg_handler = logging.FileHandler(os.path.join(CONFIG['NELMA_DIR'], 'logs', 'dbg.log'))
dbg_handler.setFormatter(FORMATTER)
CONFIG['LOGGERS']['DBG'].addHandler(dbg_handler)
