#!/bin/bash
# Reads files (absolute paths) from STDIN. Renames them to the $HASH of that file
# Usage: find ~/Data/benign/ -type f | ./rename-apks.sh
HASH=md5sum
while read -r f; do old=$f; new=$(dirname $f)/$($HASH $f | cut -d" " -f1).apk; echo "$old $new" ; mv $old $new ; done
