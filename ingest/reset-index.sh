#!/bin/bash
URL="localhost:9200"

if [ "$#" -lt 1 ]; then
  echo "Usage: $0 INDEX [INDEX [INDEX ...]]"
  echo "Reset all indexes passed as args. Warning: all data in the indexes will be deleted."
  exit 1
fi
# ES Mappings are need to allow exact string queries against fields that may include punctuation
for index in "$@"; do
    echo "Resetting $index"
    curl -XDELETE $URL/$index?pretty
    curl -XPUT -H "Content-Type: application/json" $URL/$index?pretty -d @- <<EOF
{
  "mappings":{
    "1":{
        "properties":{
          "sample_id":{
            "type":"keyword",
            "index": true
          },
          "predictor_id":{
            "type":"keyword",
            "index":"true"
          }
        }
      }
    }
}
EOF
    curl -XPUT -H "Content-Type: application/json" $URL/$index/_settings?pretty -d '{"index.mapping.total_fields.limit": 20000}'
done
