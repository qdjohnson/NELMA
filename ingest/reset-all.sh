#!/bin/bash
INDEXES="predictions samples predictors performance static-permissions static-services static-receivers static-imports static-strings static-hardware-features"

THIS_DIR=$(dirname $0)
$(dirname $0)/reset-index.sh $INDEXES

for d in predictors knowledge-integrators; do
    rm -rf $THIS_DIR/../$d/models/*
done
