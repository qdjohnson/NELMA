#!/usr/bin/env python

# Import NELMA utils
from config import CONFIG
import db_utils

from http.server import HTTPServer, BaseHTTPRequestHandler
import multipart
import ssl
from io import BytesIO

import shutil
import os
import time

import argparse

UPLOAD_DIR=os.path.realpath(os.path.join(os.path.dirname(__file__), "submissions"))


parser=argparse.ArgumentParser(description='An HTTP(S) upload endpoint for APK files. Starts the analysis process.')
parser.add_argument('-p',"--port", default=80, type=int, required=False, help="The port to listen on")
parser.add_argument('-a',"--address", default="0.0.0.0", required=False, help="The address to listen on")
parser.add_argument('-k',"--keyfile", default="key.pem", required=False, help="The path to the SSL key file")
parser.add_argument('-c',"--certfile", default="cert.pem", required=False, help="The path to the SSL certificate file")
parser.add_argument('-s',"--ssl", required=False, action="store_true", help="Enable SSL mode")
parser.add_argument("--setup-ssl", required=False, action="store_true", help="Generate SSL certificate and key, then exit")

args = parser.parse_args()



######################################################
#   Accept submissions of APK files to be analyzed
#
#   Example sumbission using curl:
#   curl -k -F '002c0d92fc0618f66dff69d90b38bff1.apk=@../Data/amd_data/GoldDream/variety1/002c0d92fc0618f66dff69d90b38bff1.apk' http://127.0.0.1/
######################################################
class SubmissionEndpoint(BaseHTTPRequestHandler):
    def _reply(self, msg):
        self.send_response(200)
        self.send_header('Content-Length', len(msg))
        self.end_headers()
        self.wfile.write(msg)
        
    # Return index.html for all GET requests
    def do_GET(self):
        msg = None
        with open('index.html', 'rb') as f:
            msg = f.read()
        self._reply(msg)

    def do_POST(self):
        host = self.client_address[0]
        print("Got upload from {}".format(host))
        localpath=None
        try:
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            stream = BytesIO(body)
            boundary = stream.readline()
            print(boundary)
            boundary = boundary.strip(b"\r\n")[2:]
            stream.seek(0)
            parser = multipart.MultipartParser(stream, boundary)
             
            for part in parser:
                localpath = os.path.join(UPLOAD_DIR, os.path.basename(part.name))
                print("Sumbmission {} uploading to {}".format(part.name, localpath))
                with open(localpath, 'wb') as f:
                    shutil.copyfileobj(part.file, f)
                
        except Exception as e:
            print(e)
            self._reply(str(e).encode())
        else:
            # Submission was successful
            sample_id = db_utils.get_sample_id(localpath)
            self._reply(b"<html>Successfully submitted sample {0}!<br>\nCheck <a href='http://localhost:9200/_search?q=sample_id:{0}'>here</a> for results!<br>\n</html>".format(sample_id))
            # WARNING: Verify this is sanitized
            os.system('../scripts/process_submission.py {}'.format(localpath))

    # Stop log messages from printing to the screen
    #def log_message(self, format, *args):
    #    return

def main():
    if args.setup_ssl:
        os.system('openssl req -nodes -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365')
        print("key.pem and cert.pem created")
        return

    httpd = HTTPServer((args.address, args.port), SubmissionEndpoint)
    if args.ssl:
        httpd.socket = ssl.wrap_socket (httpd.socket, 
                   keyfile=args.keyfile, 
                   certfile=args.certfile, server_side=True)
    
    httpd.serve_forever()

if __name__=="__main__":
    main()
