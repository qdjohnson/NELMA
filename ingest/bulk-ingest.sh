#!/bin/bash
# https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
# Let bash expand home directory for us
DIRS=$(echo ~/Data/benign ~/Data/amd_data)

find $DIRS -type f | ${SCRIPTPATH}/../utils/extractor_utils.py
