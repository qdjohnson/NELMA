#!/usr/bin/env python

# Low-level utilities for directly manipulating Elasticsearch data directly
# Usage: Import from an interactive python shell

from config import CONFIG
from elasticsearch import Elasticsearch, helpers
import pandas as pd
from pandas.io.json import json_normalize

import hashlib
import time

import requests
import json

# Abstract db operations from predictors where possible

# Return the authoritative list of columns that should not be trained on
#   (these columns are either metadata added for NELMA or directly tied to ground_truth)
def get_spoiler_cols():
    # Note: Metadata columns that are not replicated across all indexes are not included here (e.g. family)
    return ["ground_truth", "nelma_label", "training_label", "file_path", "sample_id", "hidden", "extractor_id", "sample_weight"]
    # Note: train with nelma.label as truth, evaluate with ground_truth as truth

# Returns the name of the index / table where predictions should go
def get_prediction_index():
    return "predictions" 


# Returns the name of the index / table where all sample metadata should go
def get_sample_index():
    return "samples" 

# Returns the name of the index / table where all predictor metadata should go
def get_predictors_index():
    return "predictors" 

# Returns the name of the index / table where model performance data should go
def get_performance_index():
    return "performance" 

def get_all_samples():
    return dump_index(get_sample_index())

def get_all_predictions():
    return dump_index(get_prediction_index())

# Return the connection URL to the database
def get_db_url():
    return CONFIG["ES_URL"]

# Return the database connection object to be used by all queries
def get_db_conn():
    return Elasticsearch(get_db_url(), timeout=CONFIG["TIMEOUT"])


# Returns the ES query that matches everything
def get_all_query():
    return {
        "query": _match_all()
    }

# Returns the partial ES query that matches everything
# (Can be embedded in a query or update by query
def _match_all():
    return {
        "match_all":{}
    }

# Returns the ES query that matches "hidden" instances
def get_hidden_query():
    return {
        "query": {
            "terms":{"hidden":[True]}
        }
    }

# Returns the ES query that matches "hidden" instances
def get_not_hidden_query():
    return {
        "query": {
            "bool": {
                "must_not": {
                    "terms":{
                        "hidden":[True]
                    }
                }
            }
        }
    }

# Returns a simple query for the requested _id
#   By default, hidden samples are excluded, but this can be overridden
def get_query(_id, show_hidden=False, _id_field="sample_id"):
    if show_hidden:
        return {
            "query": {
                "match": {
                    _id_field:_id
                }
            }
        }
    else: # default: sample_id matches AND not hidden
        return {
            "query": {
                "bool": {
                    "must": [
                    {
                        "match":{
                            _id_field:_id
                        }
                    },
                    {
                        "term":{
                            "hidden":False
                        }
                    }
            
                    ]
                }
            }
        }

# Returns a partial query for use by update_by_query
def _get_query_part(key, value):
    return {
        "terms":{key:[value]}
    }

# Returns a script in ES Query DSL syntax that will set key=val
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-update-by-query.html
#   https://www.elastic.co/guide/en/elasticsearch/painless/current/painless-examples.html
def get_update_query(key, val):
    return {
        "source":"ctx._source.{0}=params.{0}".format(key),
        "lang":"painless",
        "params": {
            key:val
        }
    }

# untested
def get_inc_query(key):
    return {
        "lang":"painless",
        "inline":"ctx._source.{}++".format(key),
    }

def get_math_script(key, val, op="+"):
    return {
        "source":"ctx._source.{0} {1}= params.{0}".format(key, op),
        "lang":"painless",
        "params": {
            key:val
        }
    }
def get_doc_id(index, query):
    es = get_db_conn()
    res = None
    try:
        res = es.search(index=index, body=query)
        return res['hits']['hits'][0]['_id']
    except Exception as e:
        CONFIG['LOGGERS']['DB'].error("Error - Query:\n{}\nreturned:\n{}".format(query, res))
        return None
    
def upsert(index, _id, doc, _id_field='sample_id'):
    es = get_db_conn()
    query = get_query(_id, show_hidden=True, _id_field=_id_field)

    doc_id = get_doc_id(index, query)
    if not doc_id:
        # need to insert
        if _id_field != "sample_id":
            CONFIG['LOGGERS']['DB'].info("Upsert - inserting {} to {}".format(_id, index))
        insert(index, doc)
    else:
        if _id_field != "sample_id":
            CONFIG['LOGGERS']['DB'].info("Upsert - updating {} in {}".format(_id, index))
        # need to update
        max_retries = 50
        
        es.update(index=index, doc_type="1", id=doc_id, body={'doc':doc, 'doc_as_upsert':True}, retry_on_conflict=max_retries, refresh=True)


# Update or Insert the prediction for this sample
# (Note: using an update query would probably also work)
def upsert_prediction(sample_id, prediction):
    index = get_prediction_index()   
    try:
        upsert(index, sample_id, prediction)
    except Exception as e:
        CONFIG['LOGGERS']['DB'].error("Failed to upsert prediction for sample {}".format(sample_id))
        CONFIG['LOGGERS']['DB'].error(e)
        for k,v in prediction.items():
            CONFIG['LOGGERS']['DB'].debug("{}:{}".format(k,v))
        return
 
# Label the prediction results with the predictor_id so it can be written to the database
def format_prediction(predictor_id, prediction, sample_id, tags={}):
    d = {} 
    # prefix each field with this unique PREDICTOR_ID 
    for k,v in prediction.items(): 
        d["{}.{}".format(predictor_id, k)] = v 
    d["sample_id"] = sample_id
    d.update(tags)
    return d

# Get the results of an ES query as a dataframe
def get_examples(index, query):
    es = get_db_conn()
    res = helpers.scan(client=es, scroll='2m', query=query, index=index)
    res = [r['_source'] for r in res]
    #for r in res:
    #    CONFIG['LOGGERS']['DB'].debug("Result ({}): {}\n\n".format(type(r), r))

    df = json_normalize(res)
    if df is not None:
        CONFIG['LOGGERS']['DB'].info("{} examples obtained from {}".format(len(df.index), index))
    return df

def query_by_id(_id, index):
    CONFIG['LOGGERS']['DB'].info("id: {}, index:{}".format(_id, index))
    _id_field = 'sample_id'
    if index == get_predictors_index():
        _id_field = 'predictor_id'

    q = get_query(_id, show_hidden=True, _id_field=_id_field)
    es = get_db_conn()
    CONFIG['LOGGERS']['DB'].debug("Query: {}".format(q))
    res = es.search(index=index, body=q)
    #TODO: Error checking
    try:
        CONFIG['LOGGERS']['DB'].debug(res)
        res = res['hits']['hits'][0]['_source']
        return res
    except Exception as e:
        CONFIG['LOGGERS']['DB'].error("Query by id failed: {}[{}] for id:{}".format(index, _id_field, _id))
        CONFIG['LOGGERS']['DB'].error("DB returned: {}".format(res))
        CONFIG['LOGGERS']['DB'].error(e)
        return None
    
# Get the features from a single example into a df
def get_example(sample_id, index):
    res = query_by_id(sample_id, index)
    if not res:
        CONFIG['LOGGERS']['DB'].error("Sample {} not found!".format(sample_id))
        CONFIG['LOGGERS']['DB'].error(res)
    else:
        return json_normalize(res)

# Return predictor metadata for use by predictor
def get_predictor_info(predictor_id):
    index = get_predictors_index()
    res = query_by_id(predictor_id, index)
    if res is None:
        return {}
    else:
        return res

def get_num_records(index, query):
    es = get_db_conn()
    return es.count(index=index, body=query).get('count')

def get_num_avail(index):
    return get_num_records(index, get_not_hidden_query())

# Updates all items matching the query
# Example code: https://www.wikitechy.com/tutorials/elasticsearch/partial-update-and-update-by-query-in-elasticsearch
def update_by_query(index, query, script, wait_for=True):
    es = get_db_conn()
    body = {
        "script":script,
        "query":query
    }
    
    CONFIG['LOGGERS']['DB'].debug("\nExecuting update query against {}:\n{}".format(index, body))
    #res = es.update_by_query(index=index, doc_type="1", body=body, conflicts='proceed', refresh=True, wait_for_completion=True)
    if wait_for:
        res = es.update_by_query(index=index, doc_type="1", body=body, conflicts='proceed', refresh=True, wait_for_completion=True)
    else:
        res = es.update_by_query(index=index, doc_type="1", body=body, conflicts='proceed')
    #res = es.update_by_query(index=index, doc_type="1", body=body, version=False)

    #try:
    #    CONFIG['LOGGERS']['DB'].debug("Update by query updated: {}".format(res['updated'])) 
    #except Exception as e:
    #    pass

# Insert a document into the given index
#   This method uses the HTTP(S) API of Elasticsearch to demonstrate how to do so
#   (This method is called by feature extractors, which you could implement in any language)
#   Functionally equivalent to:
#   cat my_data | curl -XPOST -H'Content-Type: application/json' ELASTIC_URL -d @-
def insert(index, doc):
    CONFIG['LOGGERS']['DB'].debug("Inserting into {} ({})".format(index, CONFIG['ES_URL_TEMPLATE'].format(index)))
    headers = {'Content-Type': 'application/json'}
    doc = sanitize(doc)
    r = requests.post("{}&refresh=wait_for".format(CONFIG['ES_URL_TEMPLATE'].format(index)), headers=headers, data=json.dumps(doc))
    ok = [200, 201]
    if r.status_code not in ok:
        CONFIG['LOGGERS']['DB'].error("Error inserting {}".format(doc))
        CONFIG['LOGGERS']['DB'].error(r.text)
    else:
        CONFIG['LOGGERS']['DB'].info("Successfully inserted {} values into {}".format(len(doc.keys()), index))
    return r.status_code in ok

# Return a sample_id given a file
#   (currently the sha256 hash is used as sample_id)
def get_sample_id(filepath):
    chunk_size = 4096
    partial_hash = hashlib.sha256()
    try:
        with open(filepath, 'rb') as f:
            chunk = f.read(chunk_size)
            while len(chunk):
                partial_hash.update(chunk)
                chunk = f.read(chunk_size)
        return partial_hash.hexdigest()
    except Exception as e:
        CONFIG['LOGGERS']['DB'].error("Could not produce sample_id for {}".format(filepath))
        CONFIG['LOGGERS']['DB'].error(e)
        raise e

# Takes in metadata from the metadata index for a particular sample (what the feature extractor sees)
#   returns the minimum metadata that should be included in the feature index
#   (e.g. sample_id and ground_truth are metadata, but should be replicated across all indexes for convience)
def clean_meta(meta):
    return {k:meta[k] for k in get_spoiler_cols() if k in meta}

# Called by feature extractors
def upload_features(index, features, meta):
    # Add any necessary metadata to the feature dict before updloading
    features.update(clean_meta(meta))
    return insert(index, features)

def get_headers():
    return {'Content-Type': 'application/json'}
    
def add_prediction_placeholder(meta):
    return requests.post(CONFIG["ES_URL_TEMPLATE"].format(get_prediction_index()), headers=get_headers(), data=json.dumps(clean_meta(meta)))

def delete_index(index):
    return requests.delete("{}/{}?pretty".format(CONFIG["ES_URL"], index) , headers=get_headers())
    
# Called by feature extractors
#   stores any meta-data needed to track this sample
#   should be called once per new sample
def register_sample(meta):
    headers = get_headers()
    meta.update({"sample_id":get_sample_id(meta["file_path"]),
            "hidden":False,
            "sample_weight":1.0
    })

    meta = sanitize(meta)

    #r1 = requests.post(CONFIG["ES_URL_TEMPLATE"].format(get_prediction_index()), headers=headers, data=json.dumps(clean_meta(meta)))
    r1 = add_prediction_placeholder(meta)
    # Add extra metadata for the samples index that we omitted in the predictions index
    meta.update({
            "@register_timestamp":time.time(),
    })
    r2 = requests.post(CONFIG["ES_URL_TEMPLATE"].format(get_sample_index()), headers=headers, data=json.dumps(meta))
    # not currently necessary
    #meta = sanitize(meta)
    CONFIG['LOGGERS']['DB'].info("done")
    ok = [200, 201]
    return r1.status_code in ok and r2.status_code in ok

def register_predictor(predictor_id, subscriptions):
    doc = {
        'predictor_id':predictor_id,
        'subscriptions':subscriptions,
        'enabled':True,
        'retrain':True,
        'times_trained':0,
        'num_seen':0,
        'num_labeled':0,
        '@register_timestamp':time.time(),
        'evaluation_mode':False,
    }
    CONFIG['LOGGERS']['DB'].info("Registering doc {}".format(doc)) 
    if not insert(get_predictors_index(), doc):
        return None
    else:
        return doc

# Called by drivers to record model metrics
def upload_performance_metrics(metrics):
    return insert(get_performance_index(), metrics)

# Called by KI with retrain=True to schedule retraining, called by predictors with retrain=False after training 
def update_retrain(predictor_id, retrain):
    #i = get_predictors_index()
    #q = _get_query_part('predictor_id', predictor_id)
    # Set retrain to True / False
    #update_by_query(i, q, get_update_query("retrain", retrain), wait_for=False)
    update_predictor(predictor_id, 'retrain', retrain)

def update_predictor_info(predictor_id, info):
    CONFIG['LOGGERS']['DB'].info("Upserting predictor_info for {}".format(predictor_id))
    upsert(get_predictors_index(), predictor_id, info, _id_field='predictor_id')

# Set predictor[key] = val where predictor['predictor_id'] == _id
# if _id is None, all predictors will match
# if index is None, the 'predictors' index is used
def update_predictor(_id, key, val, index=None):
    if not index:
        index = get_predictors_index()
    q = _get_query_part('predictor_id', _id)

    if not _id:
        q = _match_all()

    update_field(index, q, _id, key, val)

# Set sample[key] = val where sample['sample_id'] == _id
# if _id is None, all samples will match
# if index is None, the 'samples' index is used
def update_sample(_id, key, val, index=None, wait_for=True):
    if not index:
        index = get_sample_index()
    q = _get_query_part('sample_id', _id)

    if not _id:
        q = _match_all()

    update_field(index, q, _id, key, val, wait_for=wait_for)

def update_sample_weight(_id, sample_weight, index=None):
    update_sample(_id, 'sample_weight', sample_weight, index=index)

def update_field(index, q, _id, key, val, wait_for=True):
    update_by_query(index, q, get_update_query(key, val), wait_for=wait_for)

# Called by drivers.
# For the purpose of model evaluation, a driver can ask a predictor or KI to enable "evaluation_mode"
# If evaluation mode is enabled, retraining is disallowed and retrain requests from a KI are not consumed. This prevents 
# Note: it is not valid to enable evaluation_mode if no model exists

def update_eval_mode(predictor_id, eval_mode):
    update_predictor(predictor_id, 'evaluation_mode', eval_mode)

def set_eval_mode(eval_mode):
    CONFIG['LOGGERS']['DB'].info("Setting all predictors to evaluation_mode:{}".format(eval_mode))
    update_eval_mode(None, eval_mode)
    
def remove_nelma_labels(df=None, index='_all'):
    set_nelma_labels(df, None, index)

def override_nelma_labels(df=None, labels=None, index='_all'):
    if df is None:
        set_nelma_labels(None, labels, index)
    else:
        for i_row, label in zip(df.iterrows(), labels):
            i, row = i_row
            CONFIG['LOGGERS']['DB'].info("Overriding NEMLA_LABEL of {} to {}".format(row['sample_id'] , label ))
            update_sample(row['sample_id'], 'nelma_label', label, index=index)
        

def set_nelma_labels(df=None, val=None, index='_all'):
    if df is None:
        update_sample(None, 'nelma_label', val, index=index)
    else:
        for i, row in df.iterrows():
            update_sample(row['sample_id'], 'nelma_label', None, index=index)
    

# Given an index (the name of a Database table), return a dataframe containing all values
def dump_index(index):
    query = get_all_query()
    return get_examples(index, query)
   
# Given an index (the name of a Database table), return a dataframe containing all values
# (Public-facing wrapper to the internal dump_index method)
def get_df(index):
    return dump_index(index)

# Given an index (the name of a Database table), write a csv to csv_path
def get_csv(index, csv_path):
    get_df(index).to_csv(csv_path)
 
# Return a list of all indexes that are subscribed to (by a predictor in predictors)
def get_all_subscriptions():
    # This could be more efficiently implemented using aggregations
    # However, since size(predictors) << size(data), it doesn't matter much
    df = dump_index(get_predictors_index())
    # Use a dictionary to get unique values from a set of lists
    d = {}
    for i, row in df.iterrows():
        for index in row['subscriptions']:
            d[index] = True
    CONFIG['LOGGERS']['DB'].info("Subscriptions: {}".format(d.keys()))
    return d.keys()

# There can be multi KIs recognized by NELMA.
# However, they should not make conflicted changes (e.g. only one should have 'ki_relabel' permission).
# Also, KIs , in general, will not train on the outputs of other KIs
def get_all_ki():   
    df = dump_index(get_predictors_index())
    ki_list = []
    for i, row in df.iterrows():
        if 'is_ki' in row and row['is_ki']:
            ki_list.append(row['predictor_id'])
    CONFIG['LOGGERS']['DB'].info("Currently Loaded KIs: {}".format(ki_list))
    return ki_list

# Label sample_id with prediction
# (i.e. update 'nelma_label' in all indexes that predictors subscribe to ) 
# Note: you may want your KI to only relabel samples for some predictors.
# This configuration will write the new labels to all indexes (maintaining consitency)
# and then later mark the predictors for retraining (forcing them to use the new training set)
def ki_relabel(sample_id, prediction):
    # Use this to only update indexes that predictors are currently using:
    #indexes =[ get_prediction_index()]
    #indexes.extend(get_all_subscriptions())

    #indexes = '_all'

    #script = get_update_query('nelma_label', prediction['prediction'])
    #query = _get_query_part('sample_id', sample_id)
    #CONFIG['LOGGERS']['DB'].debug("Running relabel script on {}: {}\n{}".format(indexes, query, script))
    #update_by_query(indexes, query, script, wait_for=False)

    # Since developers may be adding feature extractors, by default we will relabel label **all** indexes
    #   even if no predictor is currently using it
    CONFIG['LOGGERS']['DB'].debug("Relabeling {}".format(sample_id))
    update_sample(sample_id, 'nelma_label', prediction['prediction'], index='_all', wait_for=False)

# Mark predictors for retraining.
# If predictors is None, all known predictors will be marked for retraining
def ki_retrain(predictors=None):
    index = get_predictors_index()
    if not predictors:
        # Get all known predictors
        #  Note: we could exclude all KIs (by default, KIs decide on their own when to retrain)
        #        but this could convievably be a useful way to pass messages to KIs
        df = dump_index(index)
        predictors = df['predictor_id'].tolist()

    for predictor_id in predictors:
        update_retrain(predictor_id, True)
        
def set_primary_ki(ki_id):
    ki_list = get_all_ki()
    for ki in ki_list:
        # If this is the chosen KI, give it relabel permission
        # Else, take that permission away
        update_predictor(ki, 'ki_relabel', ki == ki_id)

# Dots (.) have special meaning to Elasticsearch and will confuse it
# The intuitive behavior is for field names to be arbitrary strings

# This is not perfect, but will work in most cases
# (watch out for two keys x.y and x_y; after sanitization, one will be lost)
# Also removes any empty keys (''). These are not permitted in Elasticsearch
def sanitize(d):
    CONFIG['LOGGERS']['DB'].debug("Sanitizing {} keys".format(len(d.keys())))
    d = {(k.replace('.', '_') if isinstance(k, basestring) else k):v for k,v in d.items()}
    try:
        del d['']
    except Exception as e:
        pass

    CONFIG['LOGGERS']['DB'].debug("Sanitized keys: {}".format(len(d.keys())))
    for k, v in d.items():
        if v and type(v) is dict:
            CONFIG['LOGGERS']['DB'].debug("Recursively sanitizing {}".format(k))
            d[k] = sanitize(v)
    return d
######################################

# Mark all samples in df as "hidden", so they won't be used for training
def hide(df, index):
    _hide(df, index, True)

def unhide(df, index):
    _hide(df, index, False)

def _hide(df, index, hide):
    if len(df.index) > 0:
        prefix = "" if hide else "Un-"
        CONFIG['LOGGERS']['DB'].info("{}Hiding {} samples in {}...".format(prefix, len(df.index), index))
        for sample_id in df.sample_id:
            #update_by_query(index, _get_query_part('sample_id', sample_id), get_update_query("hidden", hide), wait_for=False)
            update_sample(sample_id, 'hidden', hide, index=index, wait_for=False)
    refresh(index)

def increment_labeled_count(num_labeled):
    return update_by_query(get_predictors_index(), _match_all(), get_math_script('num_labeled', num_labeled, op="+"), wait_for=True)


def refresh(index='_all'):
    es = get_db_conn()
    es.indices.refresh(index)

######################################

def hide_all(index):
    #query = get_all_query()
    #hide(get_examples(index, query), index)
    update_sample(None, 'hidden', True, index=index)

def unhide_all(index):
    #query = get_all_query()
    #unhide(get_examples(index, query), index)
    update_sample(None, 'hidden', False, index=index)

def unhide_n(index, n):
    df = get_examples(index, get_hidden_query())
    CONFIG['LOGGERS']['DB'].info("{} samples are hidden".format(len(df.index)))
    if n == 0:
        return
    elif n < len(df.index):
        df = df.sample(n=n)
    unhide(df, index)

#hide_all("static-permissions")
#hide_all("predictions")

#unhide_n("static-permissions", 500)
#unhide_n("predictions", 200)
