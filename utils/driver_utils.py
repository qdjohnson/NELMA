#!/usr/bin/env python

# For global config variables and path setup
# ( allows the import to work, even though the module is in parent dir )
from config import CONFIG

# For commandline arg parsing
import argparse

# For parallelism
import multiprocessing

# For dynamic import of DIR
import importlib

# Basic utils
import os
import sys
import shutil

# Basic model metrics
from sklearn.metrics import confusion_matrix, accuracy_score, balanced_accuracy_score, f1_score, precision_score, recall_score

# Delete and recreate the models directory
# (Warning: this is a low-level operation that should only be called by drivers to ensure no previously changed models are used)
def delete_models():
    for d in CONFIG['MODEL_DIRS']:
        if os.path.isdir(d):
            CONFIG['LOGGERS']['DRIVER'].warning("Deleting {}".format(d))
            shutil.rmtree(d)
        os.mkdir(d)

# Return a general "model reportcard" for binary classifiers
def get_model_metrics(y_true, y_pred, meta={}, prefix=""):
    CONFIG['LOGGERS']['DRIVER'].info("Ground Truth:")
    CONFIG['LOGGERS']['DRIVER'].info(y_true.value_counts())
    CONFIG['LOGGERS']['DRIVER'].info("Predicted Labels:")
    CONFIG['LOGGERS']['DRIVER'].info(y_pred.value_counts())

    metrics = meta
    CONFIG['LOGGERS']['DRIVER'].info("Scoring: {}".format(meta))
    tn, fp, fn, tp = confusion_matrix(y_true, y_pred).ravel()
    report = {
        'true_negative':tn,
        'false_positive':fp,
        'false_negative':fn,
        'true_postive':tp,
        'accuracy':accuracy_score(y_true, y_pred),
        'balanced_accuracy':balanced_accuracy_score(y_true, y_pred),
        'precision':precision_score(y_true, y_pred),
        'recall':recall_score(y_true, y_pred),
    }
    metrics.update({"{}{}".format(prefix, k):v for k,v in report.items()})
    return metrics

##########################
# Internal functions
##########################
def _run_func(module_name, submodule_name, func_name):
    CONFIG['LOGGERS']['DRIVER'].info("Running {}.{}.{}()".format(module_name, submodule_name, func_name))
    # each child needs to import the module 
    mod = importlib.import_module(module_name)
    if module_name in sys.modules:
        getattr( getattr(mod, submodule_name), func_name)()
    else:
        CONFIG['LOGGERS']['DRIVER'].error("{} not in sys.modules!".format(module_name))

# Unpack the pickable tuple into the full function args we need
def run_main(args):
    try:
        _run_func(args[0], args[1], 'main')
    except Exception as e:
        CONFIG['LOGGERS']['DRIVER'].error("Error running {}: {}".format(args, e))
        

# Use initializer to force child processes to ignore our argv (which isn't for them)
def _init():
    import sys
    sys.argv = []    

# Drivers can invoke this function exec_all directly
def exec_all(dirs, num_processes=None, skip_init=False):
    init_func = _init
    if skip_init:
        init_func = None
    pool = multiprocessing.Pool(processes=num_processes, initializer=init_func)

    for d in dirs:

        # The import will fail if it looks like a directory (e.g. predictors/ ) instead of a python module (e.g. predictors)
        if d.endswith(os.path.sep):
            d = d[:-len(os.path.sep)]
        CONFIG['LOGGERS']['DRIVER'].debug(d)
        # Execute the main() function of all exposed scripts in that directory
        # (everything in __all__ , which is set by __init__.py will be executed)
        mod = importlib.import_module(d)
        CONFIG['LOGGERS']['DRIVER'].info("Executing {}.main() from all in {}".format(d, mod.__all__))
        pool.map(run_main, [(d, script) for script in mod.__all__])
        # map blocks untill everything (all scripts in dir) complete

# Developers can run this file manually to force predictors or knowledge integrators to run
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--num-processes", type=int, default=0, help="Number of processes to spawn for pool. Default: # of cores")
    parser.add_argument("DIR", nargs='+', help="The directory(s) of python scripts to run with the pool. Must contain a function named main() to be executed. " +
        "Each dir is executed sequentially in order. (Dir contents are executed in parellel)")

    args = parser.parse_args()

    if not args.num_processes:
        args.num_processes = None

    # Remove our commandline args to hide them from child processes
    # Update: likely redundant with _init()
    sys.argv = []

    exec_all(args.DIR, args.num_processes)

# If we are invoked directly from the command line, parse argv and run exec_all
if __name__ == "__main__":
    main()
