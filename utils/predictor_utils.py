#!/usr/bin/env python

# Import NELMA utils
from config import CONFIG
import db_utils

# Data manipulation
from elasticsearch import Elasticsearch, helpers
import pandas as pd
from pandas.io.json import json_normalize

#import random

#General utils
import sys
import os
import time

# Used to save / load model from disk
import pickle

# Learning algorithm(s)
import sklearn
from sklearn.svm import SVC

# WIP: Convenience constructor for KIs
# Low priority since number of KIs will be small and KI stragies can vary wildly
class KI(object):
    pass
class Predictor(object):
    def __init__(self, predictor_id, subscriptions=None, is_ki=False):
        if not hasattr(self, 'info'):
            self.info = {}

        self.info.update({'is_ki':is_ki})

        if subscriptions is None:
            subscriptions = [CONFIG['DEFAULT_INDEX']]
        # for convenience, allow strings instead of lists
        elif isinstance(subscriptions, basestring):
            subscriptions = [subscriptions]

        self.is_ki = is_ki

        self.model_dir = self.compute_abspath("models")
        CONFIG['LOGGERS']['PRED'].info("Model dir: {}".format(self.model_dir))
        # A unique identifier for this predictor
        # All predictions made by this predictor
        #   are prefixed with this value
        self.predictor_id = predictor_id
        
        # Model persistence
        # - these files will contain the data needed to
        #   save / restore a model from disk
        #Feel free to create additional state files as needed
        self.model_path = self.get_path("model")
        self.data_path = self.get_path("data")
        self.state_path = self.get_path("state")

        # By default, only the first index subscribed to will be used
        # However, to remain forward-thinking, Predictor constructor
        #   expects a list
        self.subscriptions = subscriptions

        # Register if needed
        if not self.get_predictor_info():
            CONFIG['LOGGERS']['PRED'].info("Registering {}".format(self.predictor_id))
            self.register()
        else:
            CONFIG['LOGGERS']['PRED'].info("Already registered:\n{}".format(self.info))

        if self.file_exists(self.state_path):
            self.state = self.load_obj(self.state_path)
            self.state["times_invoked"] += 1
            self.state["last_invoked"] = time.time()
            self.save_obj(self.state_path, self.state)
        else:
            # State information that can be used by the model (but is not written to database)
            #   reserved for future use
            self.state = {
                'times_invoked':0,
                'last_invoked':time.time()
            }

        if self.file_exists(self.model_path):
            self.classifier = self.load_obj(self.model_path)
        else:
            # If there is no model on disk
            CONFIG['LOGGERS']['PRED'].info("Training initial model for {}...".format(self.predictor_id))
            if self.retrain():
                self.info['init_model'] = True
            else:
                CONFIG['LOGGERS']['PRED'].error("Model is in evaluation mode, but there is no model to evaluate!")
                
            # Raise expection if model was not created
            self.classifier = self.load_obj(self.model_path)


        # self.info should now contain, minimally, 'retrain' and 'enabled'

    # Register this predictor with NELMA
    def register(self):
        CONFIG['LOGGERS']['PRED'].info("Registering {}".format(self.predictor_id))
        self.info.update(db_utils.register_predictor(self.predictor_id, self.subscriptions))
        if not self.info:
            CONFIG['LOGGERS']['PRED'].error("Failed to register predcitor: {}".format(self.predictor_id))
        else:
            CONFIG['LOGGERS']['PRED'].info("Sucessfully registered: {}".format(self.info))

    def get_predictor_info(self):
        info = db_utils.get_predictor_info(self.predictor_id)
        self.info.update(info)
        CONFIG['LOGGERS']['PRED'].debug(self.info)
        return info

    # take in a directory name like "models"
    # return an absolute path to /path/to/this/dir/models    
    def compute_abspath(self, dirname):
        import inspect
        return os.path.join(os.path.dirname( os.path.abspath(inspect.getfile(self.__class__)) ), dirname)

    def get_path(self, extension):
        return os.path.join(self.model_dir, "{}.{}".format(self.predictor_id, extension))

    # Return df without any "spolier"
    # columns, e.g. ground_truth
    def remove_spoilers(self, df):

        # ground_truth, malware_family are directly associated with the target class
        # file_path and sample_id are metadata used by the system
        # ['ground_truth', 'malware_family', "file_path", "sample_id", "hidden"]

        spoilers = db_utils.get_spoiler_cols()  
        #if self.is_ki:
            # In general, a KI should not use the output of other KIs (our peers) as input.
            # (If you want to do that, consider adding "layers" of KIs to NELMA)
            #spoilers.extend(db_utils.get_all_ki())
        ki_list = db_utils.get_all_ki()

        spoilers = [c for c in spoilers if c in df.columns and not c.startswith(self.predictor_id)]
        ki_output = [c for c in df.columns if any( [c.startswith(ki_id) for ki_id in ki_list] ) ]
        if ki_output:
            CONFIG['LOGGERS']['KI'].debug("Dropping KI output: {}".format(ki_output))
            CONFIG['LOGGERS']['KI'].info("Dropping {} KI features...".format(len(ki_output)))
            spoilers.extend(ki_output)
        return df.drop(spoilers, axis=1)

    # Do any cleaning necessary
    # (should be kept minimal - if a lot of cleaning or transformation is needed, 
    # the feature extraction should be improved or a new index created in
    # the Database with the "clean" data
    def clean_data(self, df):
        # For "spoiler columns", na may have meaning
        # (e.g. NELMA has not made a prediction instead of having predicted "false")
        feature_cols = self.get_feature_cols(df.columns)
        CONFIG['LOGGERS']['PRED'].debug("Feature cols: {}".format(feature_cols))
        CONFIG['LOGGERS']['PRED'].info("Excluding {} from NA replacement".format([c for c in df.columns if not c in feature_cols]))
        CONFIG['LOGGERS']['PRED'].info("Cleaning data with shape: {}".format(df.shape))
        # Everything throws "SettingWithCopyWarning", but this is the most readable
        df[feature_cols] = df[feature_cols].fillna(value=False)

        # Looks right, still throws warning
        #df.loc[: , feature_cols].fillna(value=False, inplace=True)

        #df.loc[: , feature_cols] = df[feature_cols].fillna(value=False)

        # https://www.reddit.com/r/learnpython/comments/48rzzm/pandas_is_there_a_way_to_do_fillna_on_multiple/
        #df.loc[:,feature_cols] = df.loc[:,feature_cols].fillna(value=False)

        # Fill in "NaN" features with false (indicating "not present")
        CONFIG['LOGGERS']['PRED'].debug("Cleaned Features:\n{}".format(df.head()))
        return df

    # Evertying not a spoiler is a feature
    def get_feature_cols(self, col_list):
        return get_feature_cols(col_list)

    # The available fields may have changed since we last trained, or we may be asked to predict values for
    # instances with fields we have not yet seen.
    # To prevent errors, we drop these unknown columns before attempting classification.
    # Note: get_examples will add any features we need
    def remove_unknown_cols(self, df):
        try:
            known = pd.read_pickle(self.data_path)
            CONFIG['LOGGERS']['PRED'].info("Removing unknown cols. (known: {})".format(len(known.columns)))

            CONFIG['LOGGERS']['PRED'].debug("Before : {} features".format(len(df.columns)))
            df = df[[c for c in known.columns if c in df.columns]]
            CONFIG['LOGGERS']['PRED'].debug("After : {} features".format(len(df.columns)))
            #print("Recleaning data to remove any new NAs...")
            #df = self.clean_data(df)
            CONFIG['LOGGERS']['PRED'].debug("({} features)".format(len(df.columns)))
        except Exception as e:
            # First run; file does not exist
            CONFIG['LOGGERS']['PRED'].error(e)
            pass
        return df

    # Train a model to fit the df
    # Input: Training DataFrame
    # Output: A Trained Model with a .predict() method
    # This is an abstract method that must be implemented by inheriting classes
    def fit(self, df, labels):
        raise NotImplementedError

    # Helper method for simple predictors. Can be called by MyPredictor.fit()
    #   my_classifier = MyClassifier()
    #   self._fit(my_classifier, df, labels)
    def _fit(self, classifier, df, labels):
        #sample_weights = df['sample_weight']
        sample_weights = df['sample_weight'].to_numpy()
        # Remove any columns that reveal the target class
        X = self.remove_spoilers(df)
        CONFIG['LOGGERS']['PRED'].info("{} fitting {} training examples".format(self.predictor_id, len(X.index)))
        CONFIG['LOGGERS']['PRED'].debug("\n{}".format(X.head()))
        # Fit the model
        if hasattr(classifier, 'fit'):
            try:
                classifier.fit(X, labels, sample_weight=sample_weights)
            except Exception as e:
                CONFIG['LOGGERS']['PRED'].warning("{}:{} @{}".format(self.predictor_id, e, time.time()))
                # Maybe the .fit method does not accept a sample_weight paramater 
                classifier.fit(X, labels)

            return classifier
        else:
            CONFIG['LOGGERS']['PRED'].error("ERROR - no classifier.fit() needed by Predictor._fit. Override this function if needed.")
            # return None to indicate the failure to fit

    # Queries Elasticsearch for all avaiable data
    # Retrains the model
    # Writes the model to disk (where it can be loaded later)
    def retrain(self):
        # Input: the static-permissions index of elasticsearch
        # Output: a classification of malware / benign

        if self.info['evaluation_mode']:
            CONFIG['LOGGERS']['PRED'].warning("Predictor {} is in evaluation mode. Ignoring request to retrain".format(self.predictor_id))
            return False

        df = self.get_training_examples()
        self.info["num_seen"] = len(df.index)

        CONFIG['LOGGERS']['PRED'].debug(df.head())
        if len(df.index) == 0:
            CONFIG['LOGGERS']['PRED'].warning("No Data is availabe! Is everything hidden?")
            raise Exception("No Data Available")


        #DEBUG
        # all data obtained can be exported if needed
        #df.to_csv('data.csv')

        # Save all known column values to a file
        #   This dataframe stub will be combined with a particular sample's features
        #   The result should have all the columns that the model expects
        df.head(0).to_pickle(self.data_path)
        
        CONFIG['LOGGERS']['PRED'].info("Fitting model in {}.retrain()...".format(self.predictor_id))

        # TODO: Compute an alternate to ground_truth that uses output from Knowledge Integrator
        
        #model = self.fit(df, df.ground_truth)
        CONFIG['LOGGERS']['PRED'].info("Training Distribution:\n{}".format(df.training_label.value_counts()))
        model = self.fit(df, df.training_label)
        CONFIG['LOGGERS']['PRED'].info("Fit done")


        # Save the model for futrue use    
        self.save_obj(self.model_path, model)

        # Update the state information
        # (so that we can decide when we need to retrain)
        self.save_obj(self.state_path, self.state)


        # Update the internal modle in case it is needed
        self.classifier = model
        
        # Update predictor metadata 
        self.info['retrain'] = False
        self.info['times_trained'] += 1
        self.info['last_trained'] = time.time()
        # Strip the flag that indicates we constructed the initial model
        # (Otherwise we cannot trust that init_model => we trained during construction and retraining would be redundant)
        if 'init_model' in self.info:
            del self.info['init_model']
        db_utils.update_predictor_info(self.predictor_id, self.info)

        return True
        
    def save_obj(self, path, model):
        with open(path, "w") as f:
            f.write(pickle.dumps(model))

    def load_obj(self, path):
        with open(path, "r") as f:
            return pickle.loads(f.read())

    # Query Databasefor this sample
    # returns a dataframe containing only this sample
    # (this can be simulated by selecting the desired row
    #  from a complete dataframe)
    def get_features(self, sample_id):
        if not self.subscriptions:
            return None
        df = db_utils.get_example(sample_id, self.subscriptions[0])

        fields = pd.read_pickle(self.data_path)

        # Give this sample any features that may be missing
        #   (since the model may depend on features not present here)
        df = pd.concat([fields, df ], axis=0, ignore_index=True, sort=False)
        df = self.clean_data(df)

        #DEBUG
        CONFIG['LOGGERS']['PRED'].debug(df.head())
        return df

    def get_training_examples(self):
        df = self.get_examples(db_utils.get_not_hidden_query())
        # use nelma output if available, else use ground truth
        # (
        #   note: we are only operating on "non-hidden" samples, 
        #   so in general, nelma labels will be available after the first training
        # )
        CONFIG['LOGGERS']['PRED'].debug([c for c in df.columns if c in db_utils.get_spoiler_cols() ])
        CONFIG['LOGGERS']['PRED'].debug("Types: {}".format(df.dtypes))
        if 'nelma_label' in df.columns and 'ground_truth' in df.columns:
            df['training_label'] = df.nelma_label.combine_first(df.ground_truth)
            CONFIG['LOGGERS']['PRED'].debug(df[ ['sample_id', 'ground_truth', 'nelma_label', 'training_label']  ].head())
        elif 'ground_truth' in df.columns:
            df['training_label'] = df.ground_truth
            CONFIG['LOGGERS']['PRED'].debug(df[ ['sample_id', 'ground_truth', 'training_label']  ].head())
        else:
            CONFIG['LOGGERS']['PRED'].error("Cannot train without training labels! nelma_label or ground_truth must be set")
            raise Exception("{} cannot train - no training labels available".format(self.predictor_id))
        
        original_len = len(df.index)

        df = df[pd.notnull(df['training_label'])]

        # Cast training label to boolean (this will need to change to support multi-class classification)
        df = df.astype({'training_label':bool})

        new_len = len(df.index)
        if new_len < original_len:
            CONFIG['LOGGERS']['PRED'].warning('{} dropped {} rows without training label. {} remain.'.format(self.predictor_id, original_len - new_len, new_len ))
        
        # If there are any nelma_labels, do no train on anything that NELMA did not label
        nelma_labeled = pd.notnull(df['nelma_label'])
        if any(nelma_labeled):
            df = df[nelma_labeled]
            final_len = len(df.index)
            if final_len < new_len:
                CONFIG['LOGGERS']['PRED'].warning('{} dropped {} rows without training label. {} remain.'.format(self.predictor_id, new_len - final_len, final_len ))
            
        return df

    def get_all_examples(self):
        return self.get_examples(db_utils.get_all_query())

    # Queries Database for all available data that this predictor can use
    # Returns a dataframe containing this information
    # (can be simulated with reading from a csv or returning a dataframe from elsewhere)
    def get_examples(self, query):
        # TODO: Factor out configuration information
        #es = Elasticsearch(ES_URL)

        #res = helpers.scan(client=es, scroll='2m', query=query, index=self.subscriptions[0])
        #res = [r['_source'] for r in res]


        #for r in res:
        #    print("Result ({}): {}\n\n".format(type(r), r))

        #print(type(res))
        if not self.subscriptions:
            return None
        
        #TODO: Join over all subscriptions on column sample_id
        df = db_utils.get_examples(self.subscriptions[0], query)

        # Give these samples any features that may be missing
        #   (since the model may depend on features not present in the queried samples)
        try:
            fields = pd.read_pickle(self.data_path)
            df = pd.concat([fields, df ], axis=0, ignore_index=True, sort=False)
        except IOError as e:
            # File not found on first run is fine
            pass
        except Exception as e:
            CONFIG['LOGGERS']['PRED'].error("Error reading column data from disk:")
            CONFIG['LOGGERS']['PRED'].error(e)

        df = self.clean_data(df)
        return df 

    def format_prediction(self, pred, conf=None, details=None):
        return {
            "prediction":bool(pred),
            "confidence":conf,
            "details":details
        }


    def predict(self, df):
        predictions = self.get_predictions(df)
        confidences = self.get_confidences(df)
        details = self.get_details(df)
        # DEBUG
        CONFIG['LOGGERS']['PRED'].debug("Labels:")
        CONFIG['LOGGERS']['PRED'].debug(predictions[:5])
        CONFIG['LOGGERS']['PRED'].debug("Confidences:")
        CONFIG['LOGGERS']['PRED'].debug(confidences[:5])
        CONFIG['LOGGERS']['PRED'].debug("Details:")
        CONFIG['LOGGERS']['PRED'].debug(details[:5])
        return predictions, confidences, details

    # Override this if your classifer does not have a .predict() or you need access to "spoiler" columns
    # (assumed function names follow scikit-learn)
    def get_predictions(self, df):
        df = self.remove_spoilers(df)
        return self.classifier.predict(df)

    def get_confidences(self, df):
        df = self.remove_spoilers(df)
        if hasattr(self.classifier, 'predict_proba'):
            confidences = []
            for conf_array in self.classifier.predict_proba(df).tolist():
                # For multiclass problems, you can send the whole array.
                # But to reduce redundancy, we can set confidence = max_confidence
                #conf_dict = {str(label):conf for label, conf in zip(self.classifier.classes_, conf_array) }
                #confidences.append( conf_dict )

                confidences.append(max(conf_array))
            return confidences
        else: 
            return [None]*len(df.index)

    #def get_top_weights(self, weights, features, n=100):
    #    return zip(*sorted(zip(weights, features), key=lambda x: abs(x[0]) )[:n])


    def get_top_weights(self, weighted_features, present_features, n=10):
        decision_breakdown = sorted([(weighted_features[f], f) for f in present_features], key=lambda x: abs(x[0]) )
        return {f:w for w,f in decision_breakdown[:n] }
        

    # Return a dictionary with details about this classification result.
    #   Default: if possible, list the coefficients for variables present in this sample
    def get_details(self, df):
        df = self.remove_spoilers(df)
        details = []
        weighted_features = None
        if hasattr(self.classifier, 'coef_'):
            weights = self.classifier.coef_.tolist()[0]
            weighted_features = {f:w for f, w in zip(df.columns, weights)}

            CONFIG['LOGGERS']['PRED'].debug("Weighted Features: {}".format(weighted_features))

        for i, row in df.iterrows():
            CONFIG['LOGGERS']['PRED'].debug("Row ({}): {}".format(type(row), row))
            d = {}
            row_dict = row.to_dict()
            CONFIG['LOGGERS']['PRED'].debug("Row_Dict: {}".format(row_dict))
            # get truthy features
            present_features = [f for f,v in row_dict.items() if row_dict[f] ]
            CONFIG['LOGGERS']['PRED'].debug("Present Features: {}".format(present_features))
            if weighted_features: 
                # The details for this sample are the coeficients of the features present       
                d.update( self.get_top_weights(weighted_features, present_features) )
                CONFIG['LOGGERS']['PRED'].debug("Details: {}".format(d))
            # ... add other details for this row
            details.append(d)

        # If we couldn't extract any details..
        if not details:
            details = [None]*len(df.index)

        return details

    def retrain_if_needed(self):

        # Placeholder test "should I retrain"?
        # -- retrains if there is 10% new data available
        #    could compute how many mistakes it has made,
        #    or check for a command from the Knowledge Integrator
        #    that retraining is necessary

        # Get number of samples in index
        #num_avail = self.es.count(index).get('count')

        # Get number of non-hidden samples in index
        #num_avail = self.es.count(index=self.subscriptions[0], body=Predictor.query_not_hidden()).get('count')

        #num_avail = db_utils.get_num_avail(self.subscriptions[0])

        #print("Comparing {} vs {}".format(self.state["num_seen"], num_avail))
        #if 1.10 * self.state["num_seen"] < num_avail:
        #    print("New data is available; retraining...")
        #    self.retrain()

        CONFIG['LOGGERS']['PRED'].info("Retrain if needed: {}".format(self.info))

        if self.info['retrain']:
            self.retrain()

    # Classify a particular sample by ID
    def classify(self, sample_id):
        # Elasticsearch conneciton info

        self.retrain_if_needed()
        # Get the features of the requested sample
        df = self.get_features(sample_id)
        df = self.remove_unknown_cols(df)

        # Do the prediction
        prediction, confidence, details = self.predict(df)

        # np.bool_ is not serializable, so it should be converted to
        #   a python bool
        return self.format_prediction(prediction[0], confidence[0], details[0])

    def _classify_all(self):
        self.retrain_if_needed()

        df = self.get_all_examples()
        ## Load the model from disk
        #classifier = self.load_obj(self.model_path)

        df = self.remove_unknown_cols(df)

        labels, confidences, details = self.predict(df)

        predictions = [self.format_prediction(p, c, d) for p, c, d in zip(labels, confidences, details)]
        #print("Sample IDs: {}".format(df['sample_id']))
        #print("Predictions: {}".format(predictions))

        # TODO: move at least some of the state info into the model registry
        CONFIG['LOGGERS']['PRED'].info("{} - model was last trained on {} examples".format(self.predictor_id, self.info["num_seen"]))
        return (df['sample_id'].tolist(), predictions)


    # Classify all available samples, retraining first if marked for retraining by the KI.
    # If this is a KI, it will retrain before classiying and then mark all predictors for retraining
    def classify_all(self):
        if self.is_ki and not self.info.get('init_model', False):
            CONFIG['LOGGERS']['KI'].info("KI ({}): Retraining before classify_all".format(self.predictor_id))
            self.retrain()

        res = self._classify_all()

        if self.is_ki:
            # TODO: Protect with a ki_retrain permission
            if self.info['ki_relabel']:
                CONFIG['LOGGERS']['KI'].info("KI ({}): Marking all predictors for retraining".format(self.predictor_id))
                db_utils.ki_retrain()

        return res
    def _get_conf(self, prediction_dict):
        #return max(prediction_dict['confidence'].values())
        return prediction_dict['confidence']

    # If a KI is able to relabel, should it?
    # Returns True if yes, else False
    def should_relabel(self, prediction):
        return self._get_conf(prediction) >= self.info['relabel_threshold']

    # Submit a prediction result to the database, where the Knowledge Integrator can use it
    def submit_prediction(self, sample_id, prediction):
        # Tag this prediction with this predictor_id for the database, plus any additional categories to aid in visualization
        tags = {'is_ki':self.is_ki}
        formatted_prediction = db_utils.format_prediction(self.predictor_id, prediction, sample_id, tags)

        db_utils.upsert_prediction(sample_id, formatted_prediction)
        # Only one Knowledge Integrator should relabel at a time, but multiple can offer their opinions
        # (you can disable this check or add additional flags if you need multiple KIs or groups of KIs that work together)
        if self.is_ki:
            if self.info['ki_relabel']:
                if self.should_relabel(prediction):
                    CONFIG['LOGGERS']['KI'].info("{} relabeling {}".format(self.predictor_id, sample_id))
                    db_utils.ki_relabel(sample_id, prediction)
                else:
                    CONFIG['LOGGERS']['KI'].info("{} is not confident enough to relabel {}".format(self.predictor_id, sample_id))
                    
            else:
                CONFIG['LOGGERS']['KI'].debug("{} is a KI, but does not have permission to relabel ('ki_relabel' is False)".format(self.predictor_id))

    def is_enabled(self):
        # No info = not registered = assume enabled
        return self.info.get('enabled', True)

    def file_exists(self, path):
        return os.path.isfile(path)


# Helper method that wraps predictor.classify_all()
def classify_all(predictor):
    # Classify all examples
    tmp = predictor.classify_all()
    CONFIG['LOGGERS']['PRED'].info("Submitting {} predictions from {}...".format(len(tmp[0]), predictor.predictor_id))
    for sample_id, prediction in zip(tmp[0], tmp[1]):
        CONFIG['LOGGERS']['PRED'].debug("{}:{}".format(sample_id, prediction["prediction"]))
        predictor.submit_prediction(sample_id, prediction) 

    # Ensure all our changes are flushed to the database / searchable
    db_utils.refresh()

# Helper method that wraps predictor.classify()
def classify(predictor, samples):
    # for all samples requested

    for sample_id in samples:
        #...classify them
        prediction = predictor.classify(sample_id)

        CONFIG['LOGGERS']['PRED'].debug("PREDICTION:{}:{}".format(sample_id, prediction["prediction"]))

        #...and upload the results to the database
        predictor.submit_prediction(sample_id, prediction) 

def driver(predictor):
    try:
        if not predictor.is_enabled():
            CONFIG['LOGGERS']['PRED'].info("Predictor {} is disabled".format(predictor.predictor_id))
            return

        # If no sample_ids are passed, classify everything
        if len(sys.argv) > 1:
            # for all samples_ids passed as arguments
            classify(predictor, sys.argv[1:])
        else:
            classify_all(predictor)
            if predictor.is_ki:
                # If the KI has classified the entire dataset, we should probably mark all predictors for retraining
                db_utils.ki_retrain()
        CONFIG['LOGGERS']['PRED'].info("DONE -- Predictor {}".format(predictor.predictor_id))
    except Exception as e:
        CONFIG['LOGGERS']['PRED'].error("Fatal error running predictor {}:\n{}".format(predictor.predictor_id, e))
        import traceback
        CONFIG['LOGGERS']['PRED'].error("".join(traceback.format_exception(*sys.exc_info())) )

# Called by Jupyter notebooks, where we do not control argv directly
def run_model(predictor):
    sys.argv = []
    return driver(predictor)

def get_feature_cols(col_list):
    # filter out spoilers cols
    cols = [c for c in col_list if c not in db_utils.get_spoiler_cols()]
    # filter out "raw" columns that most classifiers won't be able to understand
    # (override this if your classifier wants them)
    cols = [c for c in cols if not c.startswith('raw_')]

    ki_list = db_utils.get_all_ki()

    ki_output = [c for c in col_list if any( [c.startswith(ki_id) for ki_id in ki_list] ) ]
    cols = [c for c in cols if c not in ki_output]
    return cols

# Modified version of Predictor.get_training_examples suitable as a convenience method for Jupyter notebooks
# index is the name of the Database index / table to get
# returns df, labels (a pandas Dataframe with all spoilers removed and a Series containg the corresponding labels)
# if ignore_nelma is True, then nelma_label will be ignored (ground_truth will be used instead)
# Otherwise, nelma_label will take presidence and function as the ground_truth

# If only_nelma is True, the labels returned will be from nelma_label alone, not COALESCE(nelma_label, ground_truth)
# If only_nelma is False, the labels returned will be COALESCE(nelma_label, ground_truth) (i.e. nelma_label will take priority, but ground_truth is returned if there is no nelma_label)

# If drop_unlabeled, any unlabeled sample (according to the logic above) will be dropped before returning
# Otherwise, null values in the labels object indicate samples for which there is no label available or for which NELMA did not produce a label, depending on the other settings
def get_training_examples(index, ignore_nelma=True, only_nelma=False, drop_unlabeled=True):
    df = db_utils.get_df(index)

    if not ignore_nelma or only_nelma:
        if only_nelma:
            df['training_label'] = df.nelma_label
        else:
            df['training_label'] = df.nelma_label.combine_first(df.ground_truth)
    else:
        df['training_label'] = df.ground_truth
    

    if drop_unlabeled:
        df = df[pd.notnull(df['training_label'])]

    # Cast training label to boolean (this will need to be overriden to support multi-class classification)
    df = df.astype({'training_label':bool})

    labels = df.training_label        
    feature_cols = get_feature_cols(df.columns)
    df = df.loc[:, feature_cols]

    # Replace NA with False (appropriate for boolean vectors) 
    df = df.fillna(value=False)
    return df, labels

# To implement a new classifier:
#   Inherit from Predictor
#   Write a constructor
#   Write a fit method
#   Include this code at the bottom of your file:

#def main():
#    predictor = My_Predictor()
#    predictor_utils.driver(predictor)
#
# Allow direct invocation
#if __name__ == "__main__":
#    main()

