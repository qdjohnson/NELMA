#!/usr/bin/env python
from config import CONFIG

import db_utils

# Import the entire extractors directory
import extractors

import sys
import os
import time

import multiprocessing

# Default to # of cores
NUM_PROCESSES = None

# Take in an iterable and return a feature dictionary with each value in iterable set to True
def featurize_bool(iterable):
    try:
        return {i:True for i in iterable}
    except TypeError as e:
        return {}

# Take in an iterable and return a feature dictionary with each value in iterable set to the frequency of that item in iterable
def featurize_freq(iterable):
    try:
        return {i:iterable.count(i) for i in iterable}
    except TypeError as e:
        return {}

# Given a frequency dictionary d, increment the count of key k
def inc_freq(d, k):
    d[k] = d.get(k, 0) + 1 # Call func (no args) and log any exceptions
#   returns func() , or None if func() raises an exception
def safe_call(func, name=None):
    try:
        return func()
    except Exception as e:
        if not name:
            name = func.__name__
        CONFIG['LOGGERS']['EXTRACT'].error("Exception thrown in {} -- ignoring".format(name))
        CONFIG['LOGGERS']['EXTRACT'].error(e)
        #TODO better logging

# Use location of sample in file system to determine ground truth and metadata
#   (e.g. if it is in the "benign" folder, label it benign)
#   returns a dictionary with ground_truth
#   and any metadata that can be extracted from the filepath
def get_metadata(filepath):
    meta = {
        "file_path":filepath,
        "nelma_label":None
    }

    if CONFIG["MALWARE_REGEX"].search(filepath):
        CONFIG['LOGGERS']['EXTRACT'].info("MALWARE_REGEX matched")
        meta["ground_truth"] = True

        # Some malware datasets will have malware categorized by family
        #   retain this as metadata
        pattern = "{0}amd_data{0}".format(os.path.sep)
        if pattern in filepath:
            # Use "family" instead of "malware_family" since benign apps may have categories, too
            meta['family'] = os.path.dirname(filepath.split(pattern)[1])
            meta['dataset'] = "amd_data"

    elif CONFIG["BENIGN_REGEX"].search(filepath):
        CONFIG['LOGGERS']['EXTRACT'].info("BENIGN_REGEX matched")
        meta["ground_truth"] = False
        # There is currently only one benign dataset
        # (~100 apps downloaded from google play)
        meta["dataset"] = "top_google"

    else:
        CONFIG['LOGGERS']['EXTRACT'].info("{} is a user sample ".format(filepath))
        # This is a user sample
        # You can embed metadata in the filepath
        # e.g. filename = $(date +%Y%m%dT%H%M%S).$(sha256sum /path/to/file).apk

        # Extract timestamp from filename
        try: 
            base = os.path.basename(filepath)
            meta["submission_timestamp"] = base.split(".")[0]
        except Exception as e:
            # fail and continue; timestamp is not important
            CONFIG['LOGGERS']['EXTRACT'].error("Failed to retrieve timestamp from {}".format(filepath))
            CONFIG['LOGGERS']['EXTRACT'].error(e)

        meta['dataset'] = "user_submission"
     
    return meta

# ? Called by feature extractors - could be, but not recommended
# Defers to db_utils
# index = name of featureset
# features = actual feature dict
# meta = the return value of get_metadata for this sample
def upload_features(index, features, meta):
    CONFIG['LOGGERS']['EXTRACT'].info("Submitting {} features to {} for {}".format(len(features.keys()), index, meta['sample_id']))
    if not db_utils.upload_features(index, features, meta):
        CONFIG['LOGGERS']['EXTRACT'].error("ERROR Submitting featureset to {} for {}".format(index, meta['sample_id']))


# Given an extractor (a script in nelma/extractors) and a sample (identified by filepath),
#   call extractors.extractor.main(filepath) to do the feature extraction
def extract(extractor, filepath):
    CONFIG['LOGGERS']['EXTRACT'].info("Processing {} with {}".format(filepath, extractor))
    # dynamically access module attribute by string value
    features = getattr(extractors, extractor).main(filepath)
    CONFIG['LOGGERS']['EXTRACT'].info("--DONE: Processing {} with {}".format(filepath, extractor))
   
    if features:
        # The feature extractor wants default feature uploading
        meta = get_metadata(filepath)
        # Add the extractor_id (name) to this record
        #   conceivably, you could have 1 index populated by several extractors
        meta['extractor_id'] = extractor
        meta['sample_id'] = db_utils.get_sample_id(filepath)

        # Feature structure: outermost keys are index names
        # { "feature_set_id_1":{FEATURE_DICT_1}, "feature_set_id_2":{FEATURE_DICT_2} ...  }
        CONFIG['LOGGERS']['EXTRACT'].info("Featuresets extracted ({}):".format(meta['sample_id']))
        for k in features.keys():
            CONFIG['LOGGERS']['EXTRACT'].debug("{}: ({} values)".format(k, len(features[k].keys())))

        for index, feature_dict in features.items():
            CONFIG['LOGGERS']['EXTRACT'].info("Upload features {}".format(index))
            upload_features(index, feature_dict, meta)
            CONFIG['LOGGERS']['EXTRACT'].info("Upload features {} - done".format(index))
# Lambdas can't be pickled, so it's hard to submit them to a multiprocessing pol. This is a workaround
# Try to make sure that this cannot throw an exception, as it may cause the pool to hang
# https://stackoverflow.com/questions/15314189/python-multiprocessing-pool-hangs-at-join
def _extract(args):
    try:
        extract(args[0], args[1])
    except Exception as e:
        CONFIG['LOGGERS']['EXTRACT'].error("Exception occurred processing: {}".format(filepath))
        CONFIG['LOGGERS']['EXTRACT'].error(e)
        try:
            import traceback
            traceback.print_last()
        except Exception as e:
            pass


# Register the sample an submit extractor(sample) to process pool for all extractors
#   (this function should not block long -- it only submits the work to the process pool)
def submit_sample(filepath, pool, extractor_list):
    try:
        meta = get_metadata(filepath)

        if db_utils.register_sample(meta):
            CONFIG['LOGGERS']['EXTRACT'].info("Registered {}\t({})".format(filepath, meta['sample_id']))
            # Use a dynamically created function to "bake in" the filepath argument.
            #  -- extract takes 2 args, but map requires 1 arg, so define a new function that takes 1 arg
            #  This is logically the same as copy and pasting the extract fucntion but hardcoding in the filepath

            # Ask all feature extractors to process this file
            CONFIG['LOGGERS']['EXTRACT'].info("Queueing jobs for {} , using extractors: {}".format(os.path.basename(filepath), extractors.__all__))
            
            args = [(e, filepath) for e in extractor_list]
            pool.map_async(_extract, args)
        else:
            CONFIG['LOGGERS']['EXTRACT'].error("ERROR: Failed to register {}".format(filepath))

    except Exception as e:
        CONFIG['LOGGERS']['EXTRACT'].error("Uncaught Exception occurred processing: {}".format(filepath))
        CONFIG['LOGGERS']['EXTRACT'].error(e)
        try:
            import traceback
            traceback.print_last()
        except Exception as e:
            pass

# Read a list of file paths from stdin
def read_sample_paths():
    return [str(sample).strip() for sample in sys.stdin]

# Like the single_extractor_driver, but without multiprocessing to make debugging easier
def debug_extractor_driver(extractor_name):
    samples = read_sample_paths()
    CONFIG['LOGGERS']['EXTRACT'].info("\nRunning extractor {} in debug extractor mode".format(extractor_name))
    for s in samples:
        CONFIG['LOGGERS']['EXTRACT'].info("Processing sample: {}".format(s))
        extract(extractor_name, s)

# Driver to run if extractor is invoked directly instead of through NELMA's bulk extraction (default)
def single_extractor_driver(extractor_name):
    CONFIG['LOGGERS']['EXTRACT'].info("Running extractor {} in single extractor mode".format(extractor_name))
    CONFIG['LOGGERS']['EXTRACT'].info("Feed absolute filepaths of samples to STDIN. Send EOF (CTRL+D) to begin processing")
    return driver([extractor_name])

def driver(extractor_list, samples=None):
    start_time = time.time()
    if not samples:
        samples = read_sample_paths()

    if samples:
        pool = multiprocessing.Pool(processes=NUM_PROCESSES)
        CONFIG['LOGGERS']['EXTRACT'].info("Processing {} samples...".format(len(samples)))
        for sample in samples:
            submit_sample(sample, pool, extractor_list)
        CONFIG['LOGGERS']['EXTRACT'].info("\n\n{0}\n\nWaiting for all workers to finish...\n\n{0}\n\n".format('-'*80))
        pool.close()
        pool.join()
        CONFIG['LOGGERS']['EXTRACT'].info('\n\n'+'-'*80+'\n{} samples have been processed'.format(len(samples)))
        t = time.time() - start_time
        #https://stackoverflow.com/questions/3620943/measuring-elapsed-time-with-the-time-module
        t = time.strftime( "%H:%M:%S", time.gmtime(t) )
        CONFIG['LOGGERS']['EXTRACT'].info("Elapsed time: {}".format(t))
    else:
        CONFIG['LOGGERS']['EXTRACT'].warning("Pass absolute file paths to samples via STDIN")

if __name__ == "__main__":
    #Usage: find /path/to/apks/ -type f | ./this.py
    driver(extractors.__all__)
