# A Framework for NELMA: The Never-Ending Learner for Malware Analysis

Primary Developer: Joe Bivens
M.S. Student, Tennessee Tech University

## Motivation

The availability of potentially malicious software continues to outpace the security industry`s capacity for manual analysis. Machine learning models can provide highly accurate malware classification, but often rely on small datasets labelled by humans. Additionally, obfuscation techniques widely employed by malware authors may conceal some malicious attributes from analysis.

## Background

 Inspired by NELL (Never-Ending Language Learner) [https://www.aaai.org/ocs/index.php/AAAI/AAAI10/paper/viewFile/1879/2201], NELMA: the Never-Ending Learner for Malware Analysis — aims to provide a continually learning system that is primarily self-sufficient (requiring little human interaction) and integrates knowledge from multiple learning agents that utilize different feature representations. This work describes the framework built to support NELMA and future research in related areas. The result of this project is a flexible, modular, and extensible testbed for use by researchers, supporting future work in machine learning, malware analysis, data visualization, and software engineering. In its reference configuration, NELMA processes potentially malicious Android applications (APKs), is primarily implemented in Python (supporting popular libraries such as Androguard and scikit-learn), and uses Elasticsearch as a back-end database. At the core of the design philosophy behind this framework is the dedication to ensuring that NELMA can be extended modularly by providing a researcher-friendly abstraction layer that allows researchers and developers to focus on the tasks of interest to them. 


## This Project


The primary goal of this project is to design and develop a flexible, modular, extensible framework enabling machine learning research in the domain of malware detection and analysis. Furthermore, the framework should be designed with compatibility for never-ending learning, where previously learned knowledge supports learning subsequent knowledge without heavily relying on human intervention.

## Design

In general, for each APK sample submitted to the testbed, we will eventually perform the following processing phases:

1. **Feature Extraction**
    - Metadata and code features are obtained 
2. **Classification by Predictors**
    - The static classifier(s) make initial prediction
3. **Classification by Knowledge Integrator(s)**
    - Dynamic analyzer provides expert classification
4. **Feedback and Model Improvement**
    - Feedback is incorporated, improving the Predictor model(s) 


# Project Overview

NELMA’s framework ![http://bit.ly/NELMA_Diagram](http://bit.ly/NELMA_Diagram)
is designed to be extremely flexible and
modular. Where possible, very few assumptions are made about individual
components. To ensure that NELMA can continue to be extended and evolve
to meet the needs of future researchers, requirements and expectations
are intentionally lax. The default behavior provided by external-facing
convenience functions is designed to be reliable and appropriate for
most expected use cases, but future researchers are encouraged to adapt
their NELMA components as needed to meet their needs. As such, the
interfaces and expectations described here should be treated as best
practices, recommendations, and warnings, not as immutable directives.
Additionally, the majority of these component descriptions will remain
rightfully abstract (as the framework implemented is agnostic to many
implementation-specific details such as the programming language used),
but concrete implementation details and examples will be provided for
each component to aid future developers. NELMA, as an abstract
framework, is agnostic to libraries or languages used in individual
components.


## Feature Extractors

The Feature Extractors of NELMA make up its "lowest" level, interacting
directly with the applications submitted to NELMA.

### Description and Expectations

A feature extractor accepts a sample (e.g. an Android Application / APK
file) and returns a set of features that it observed while processing
that sample. The Feature Extractor is logically responsible for ensuring
that the features it observed are submitted to the feature database. In
implementation, Feature Extractors are Python scripts stored in the
directory. Extractors define a function that accepts a filepath to the
APK file to be analyzed. The main function need only return a Python
dictionary in this basic format:
```
{
  'feature_set_a': {
   'feature_a':2,
    'feature_b':False
 }

 ...
 
  'feature_set_N': {
   'feature_X':'X_string',
    'feature_b':1.24
  }

}
```

A single extractor can return multiple sets of features. In fact, this
is recommended if there is a high fixed cost associated with extracting
features from an application (e.g. the app must be read into memory and
disassembled) but multiple logically distinct sets of features can be
extracted at lost cost once the initial processing is done. provides a
clear example of this. Each key in the outermost dictionary will be
treated as the name of a database index (the Elasticsearch equivalent of
a table). When feature extraction is needed, each extractor’s `.main(sample)` 
function will be nvoked. NELMA utilizes a process pool to provide simple parallelism of feature extraction. In general, each combination of extractor and submitted sample will be submitted to a multiprocessing pool as a distinct job that can be executed concurrently. As analyzing an application may be resource intensive, the process pool used defaults to the number of processors in the system running NELMA. The implications of this parallelism is as follows: a feature extractor should not make assumptions about when or if other extrators will run, nor should a feature extractor extract more than one feature set, unless it will save a significant amount of work to do so. Otherwise, the default parallelism inherent in NELMA's design will not be able to efficiently parallelize feature extraction tasks. Utility functions, extractor-specific database abstractions, and the drivers that execute extractors are available in `utils/extrator_utils.py`. To allow individual extractors to be almost completely agnostic to NELMA, utility functions invoked here (or in `utils/db_utils.py` ) decorate and format the returned feature sets correctly for use by other components.

### Adding a New Feature Extractor

To add a new feature extractor, simply create a new Python script in the `extractors` directory and implement the `main` function as described above. Use `extractors/extrator_androguard.py` as an example. Relevant utility functions can be found (and added) in `utils/extractor_utils.py`. For example, utilizies have been written to convert an iterable list of observed features into either a dictionary with each feature assigned a value of  `True` or a value equal to the number of times the item appears in the list. While not strictly required, it is a good idea to add any new indexes (the key names used in the outermost dictionary returned by the extractor) to `ingest/reset-all.sh` (so that any manual database resets will also impact your feature sets) and to execute `./ingest/reset-index.sh` `NEW_INDEX_NAME` before use ensure the default index configuration used by NELMA is applied to your feature set as well. 


## Predictors

Predictors are machine learning agents that train on feature sets extracted by Feature Extractors and produce classifications.

### Descritpion and Expectations

A Predictor is expected to query the Feature Database for training examples, requests from the Knowledge Integrator (described below) to retrain, and any additional state information necessary. All predictors are registered with NELMA in the `predictors` index and are expected to update their registry entry with state information (e.g. the number of training examples seen, time of last training episode, number of times retrained, etc.). In practice, all predictors are located in `predictors/` and are Python classes that inherit from `utils/predictor_utils.Predictor`. Predictors eventually upload predictions (see Fig 3.3 for formatting) to the Knowledge Database. In practice, the default class methods use `scikit-learn`'s [6] function naming conventions to make producing this output simple. By default, the prediction fields will be filled with values from path{classifier.predict(...)`, the confidence fields will be populated from `classifier.predict_proba(...)` (if available), and the details will be populated from `classifier.coef_` (if available). Note: by default, the top 10 most significant features that were present in this sample are returned to allow each `details` section to be potentially different for a single model. Experimentation is needed to determine the effectiveness of this feature set, and modifications (e.g. top 5 features not present + top 5 features present) may be more appropriate to capture the absence of discriminate features. Because of the current support for `scikit-learn`, it is the recommended machine learning library for simple NELMA Predictors, but additional base Predictor classes can be added by future researchers as other libraries are needed.

### Adding a New Predictor

This parent class `predictor_utils.Predictor` provides a plethora of default functions that are appropriate for the majority of machine learning classifiers, making it easy to build Predictors that are compatible with NELMA. The defaults do, however, assume that the dataset used is logically a vector of boolean or numeric inputs. Other, more diverse feature sets can be used, but the processing needed will likely be specific to that learner and not readily reusable. Predictors are expected to be a subclass of `predictor_utils.Predictor` and to export a `main()` function that constructs the predictor and invokes `predictor_utils.driver(predictor)`. Additionally, it is expected that Predictors will implement their own `fit(self, df, labels)` method, where `df` is a `pandas.DataFrame` [5] and `labels` is a `pandas.Series`-like object that contains the appropriate training labels. By default, this method will be called whenever your model as been requests to retrain by the NELMA Knowledge Integrator (described below). Again, it is possible to create a new Predictor by simply placing a python script in the `predictors/` directory. `predictors/predictor_svm.py` is a good example to use. Make sure to set `predictor_id` to a unique value to prevent conflicts with other predictors. By starting with an existing predictor as an example, it's possible to create a new, working predictor that is fully integrated with NELMA by changing a handful of lines of code.


## Knowledge Integrators

Knowledge Integrators are special Predictors that use the Knowledge Database (implemented as the `predictions` index) for training data (instead of the Feature Database). They aggregate the results of all predictors to produce a final prediction. After making this prediction, Knowledge Integrators (or KIs) have the power to relabel potentially unlabeled samples they they have high confidence in or otherwise provide feedback to individual Predictors.

### Description and Expectations

In practice, KIs are Predictors in the `knowledge_integrators` directory. During construction, they accept a `confidence_threshold`. If a given prediction meets the confidence threshold, the KI will assign a `nelma_label` to the sample across all feature sets. By default, Predictors and KI will use the value of `nelma_label` over the value of `ground_truth` if both are present. (This allows experiments to be conducted on "unlabeled" data without needing to delete `ground_truth` labels). To allow multiple KIs (or KIs with different hyper-parameters, etc.) to be compared more easily, using multiple KIs is fully supported. However, it is expected that only one KI will be given the `ki_relabel` permission (stored in the `predictor` index) at a time. This prevents multiple KIs from making conflicting relabeling decisions. In general, all KIs are executed after all Predictors have finished submitting their predictions. This means that KIs can rely that all Predictors that were able to have weighed in on each example. In addition to relabeling samples, a KI can re-weight samples individually across different feature sets. A KI could identify a predictor that is "under-performing" (e.g. has a low level of agreement with the KI), query the `predictor` registry for that predictor's `subscriptions` (the indexes it will query) and then modify the `sample_weight` field in those indexes. Knowledge integration strategies more complex than simple relabeling is left for future work, as there is ample room for novel machine learning research. To ensure this framework can support these complex strategies, the Knowledge Integrator has permission to make arbitrary changes to the database. For example, a KI may modify feature sets to add or remove features, create new feature sets and edit the subscriptions of under-performing models, edit model hyper-parameters that a Predictor exposes in the `predictor` registry, or perform its own experiments (e.g. pragmatically adding or removing predictors to try to increase performance).

### Adding a New Knowledge Integrator

As mentioned above, multiple Knowledge Integrators can be used in NELMA at once, but care should be taken to ensure conflicting database changes are not made. Since the Knowledge Integrator with `ki_relabel` permission is able to make changes to the database that drastically alter Predictor behavior, care should be taken when deciding to perform such changes. For example, if a KI decided to label all samples as `True` at any time step, all Predictors would respect that labelling and all subsequent labels produced by individual Predictors or NELMA would also be `True`. Additionally, by default, `nelma_label` is used as the authoritative truth value when training KIs as well, meaning that a poorly configured KI may reinforce and amplify mistakes it has made in the past. Experimentation and novel machine learning research is needed in this area. The example experiment described in Chapter 5 (and implemented in `drivers/cotraining_driver.py`) periodically introduces labeled samples to NELMA that contain both classes and have a higher-than-default initial weight in an attempt to prevent such issues. (During testing of the co-training driver, NELMA never converged to a single class prediction). 

## Running NELMA

As with all aspects of NELMA's framework, what it means to "run NELMA" is flexible.

The primary use case that was implement to demonstrate NELMA is in an "ready-to-use" state is a experiment driver. Experiment drivers are designed to exercise most or all NELMA components in order to collect data (e.g. model performance metrics). As any machine learning researcher using NELMA will eventually want to collect quantitative results for their research, `drivers/cotraining_driver.py` was implemented as an end-to-end example of performing machine learning experiments with NELMA. Note: demonstrating that NELMA is capable of out-performing single classifiers or is able to perform meaningful learning without a significant number of labeled examples is likely novel research and very much beyond the scope of this project. The driver provided (and described in Chapter 5) simply demonstrates NELMA functionality as a framework and its ability to support non-trivial experimental methodology.


