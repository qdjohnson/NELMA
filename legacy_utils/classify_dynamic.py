#!/usr/bin/env python3

import random

# return boolean b with probability p
# else, return not b
def rand_noise(b, p):
    r = random.uniform(0, 1)
    if r <= p:
        return b
    else:
        return not b


def get_classification(sample):
    print("<not yet implemented>")
    r = random.uniform(0, 1)
    return r <= 0.5
        
def get_expert_classification(sample, ground_truth=None, prob=None):
    if ground_truth is not None:
        print("Simulating Dynamic Classifier with Accuracy={}".format(prob))
        if prob is None:
            return ground_truth
        elif type(prob) == "list" or type(prob) == "tuple":
            if ground_truth:
                return rand_noise(ground_truth, prob[0])
            else:
                return rand_noise(ground_truth, prob[1])
        elif prob is not None:
            return rand_noise(ground_truth, prob)
        else:
            return ground_truth
    else:
        # check for previous dynamic analysis result, else...
        print("Doing Dynamic Analysis")
        return get_classification(sample)
