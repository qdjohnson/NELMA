#!/usr/bin/env python
import fileinput
import sys
import os

import androguard
from androguard import misc

import multiprocessing
import json
import requests

# Default to # of cores
NUM_PROCESSES = None

ELASTIC_URL = 'http://localhost:9200/{}/1?pretty'
PERMISSIONS_INDEX="static-permissions"
PREDICTIONS_INDEX="predictions"
HEADERS = {'Content-Type': 'application/json'}

def get_permissions(filename, requested_features):
    a, d, dx = androguard.misc.AnalyzeAPK(filename)

    features  = {f:"" for f in requested_features}

    for f in requested_features:
        try:
            features[f] = getattr(a, f)()
        except Exception as e:
            print("Error on Feature {}: {}".format(f,e))
    return features

def process_apk(filename):
    try:
        requested_features = [
            'get_permissions',
        ]
        features = get_permissions(filename, requested_features)

        # Process the features and format as needed:

        # Featurize the get_permissions output
        for permission in features['get_permissions']:
            features[permission]=True

        del features['get_permissions']
        features["file_path"] = filename
        features["hidden"] = False
        # TODO: make this logic more generic
        if "/benign/" in filename:
            features["ground_truth"] = False 
        else:
            features["ground_truth"] = True
            features["malware_family"] = os.path.dirname(filename.split("/amd_data/")[1])
        # md5 hash of file
        features["sample_id"] = os.path.basename(filename.rsplit(".", 1)[0])
        
        #curl -XPOST -H'Content-Type: application/json' ELASTIC_URL -d @-
        r = requests.post(ELASTIC_URL.format(PERMISSIONS_INDEX), headers=HEADERS, data=json.dumps(features))
        data = {"sample_id":features['sample_id'],
                "ground_truth":features['ground_truth']
        }
        r = requests.post(ELASTIC_URL.format(PREDICTIONS_INDEX), headers=HEADERS, data=json.dumps(data))

        print("Ingesting {}:\t {}".format(filename, r.status_code))
        if r.status_code not in [200, 201]:
            print(r.text)
    except Exception as e:
        print("Exception occured processing: {}".format(filename))
        print(e)

if __name__ == "__main__":
    #Usage: find /path/to/apks/ -type f | ./this.py
    pool = multiprocessing.Pool(processes=NUM_PROCESSES)
    apks = [apk.strip() for apk in fileinput.input()]
    print("Processing {} apks...".format(len(apks)))
    pool.map(process_apk, apks)
    print('\n\n'+'-'*80+'\n{} apks have been processed'.format(len(apks)))
