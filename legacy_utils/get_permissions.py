#!/usr/bin/env python3

import pandas as pd

import ast

# Let's make a function that can clean one permission (that is a string)

def clean_permission(p):
    # Android "built-in" permissions start with android.permission
    #  developers might have defined custom permissions with their own prefix, though
    #  This function removes the "android.permission." from the start of a string, if it is present
    prefix = "android.permission."
    if p.startswith(prefix):
        # This is "string slicing"
        #   str[start_index:end_index]
        return p[len(prefix):]
    else:
        # this is some custom permission - maybe that is interesting?
        return p



data = pd.read_csv("amd_data_permissions.csv")

print("\n\n--Column labels, i.e. values of x where data[x] is valid--")
print(data.columns)

print("\n\n--A data series / list of rows for column 'get_permissions'--")
print(data['get_permissions'])

print("\n\n--Permissions for first sample--")
print(data['get_permissions'][0])

# Example:
# ['android.permission.INTERNET', 'android.permission.ACCESS_NETWORK_STATE', 'android.permission.READ_PHONE_STATE', 'android.permission.ACCESS_WIFI_STATE']
#  looks like a python list...





########################    The Python Way     ####################################
# For each item in data['get_permissions']
#  i.e. for each malware sample, get the list of permissions
for permissions in data['get_permissions']:
    # Commented out because it is messy -- 
    #print("Permission Set: {}".format(permissions))
    print("Type of permissions is {}".format( type(permissions)  ) )
    # oops, turns out it *is* a python list, but converted to a string

    # ast.literal_eval(x) converts strings that look like python literals (lists, dictionaries, etc.) into actual lists, dictionaries, etc.
    try:
        permissions_list = ast.literal_eval(permissions)
    except Exception as e:
        # If we have a bunch of data, we probably don't want to crash if one row is messed up
        print("Invalid Permissions String: {} ".format(permissions)) 
        # Skip this row and go on
        continue  
 
    print("Type of permission_list is {}".format( type(permissions_list)  ) )
    # Yay, now it is a list of strings! which is easy to work with

    # Let's apply the cleaning function to this list of strings

    # This is "list comprehension" - it works like set notation in math
    # Here, it just "cleans" every string in the list
    permissions_list = [clean_permission(x) for x in permissions_list]    
    
    print("First 5 Cleaned Permissions (as a list): {}...".format(permissions_list[:5])) 
    print("This sample has {} permissions".format(len(permissions_list)))


    # We can also make this into a string / smash it together, if you want:
    delim = " "
    permissions_string_clean = delim.join(permissions_list)
    
    print("First 50 characters of cleaned string: {}...".format(permissions_string_clean[:50] ) )


    # Okay, cool! But that is the "pure python" way to do things, not the pandas way.


################## The Pandas Way ######################################

#(note: we are outside the for loop now, this is an alertative to the stuff above:

print("\n\n------------------------------------------\n\n")

# Same idea, but we really just want to make a *new* column in our pd.DataFrame object that contains the final / cleaned version of the permissions column

# There are some extra rows somehow that have 'get_permissions' as the value :/ We should get rid of them:
# https://stackoverflow.com/questions/13851535/how-to-delete-rows-from-a-pandas-dataframe-based-on-a-conditional-expression
#   (otherwise the next step will throw an error)
data = data.drop(data[data.get_permissions == "get_permissions"].index )

# Call the ast.literal_eval function on each row. Pass the function row['get_permissions']
data['permissions_list'] = data.apply(lambda row: ast.literal_eval(row['get_permissions']) , axis=1)

# Okay, now permissions_list is a column in our DataFrame (we have one per row - each row is a malware sample)

print("\n\npermissions_list Column (before cleaning): ")
print(data['permissions_list'])


# Now let's clean it, like before!
data['permissions_list'] = data.apply(lambda row: [clean_permission(x) for x in row['permissions_list']] , axis=1)

# Woah, that one was kinda tricky!
#       row['permissions_list'] is our python list of permissions
#       [clean_permission(x) for x in row['permissions_list']] makes a NEW list, by cleaning each permission in the old list


# Okay, now permissions_list is a clean list of strings and a column in our DataFrame (we have one per row - each row is a malware sample)

print("\n\npermissions_list Column (after cleaning): ")
print(data['permissions_list'])



# We can also make it into a string, if that is easier to work with:

delim = ' '    
# Call " ".join function on each row. Pass the function row['permissions_list']
data['permissions_string_clean'] = data.apply(lambda row: delim.join(row['permissions_list']), axis=1)

#Tada!

print("\n\npermissions_string_clean Column: ")
print(data['permissions_string_clean'])

