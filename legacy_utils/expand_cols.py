#!/usr/bin/env python3

import pandas as pd

import ast

# https://developer.android.com/guide/topics/permissions/overview
#PROTECTION_NORMAL
normal = """
ACCESS_LOCATION_EXTRA_COMMANDS
ACCESS_NETWORK_STATE
ACCESS_NOTIFICATION_POLICY
ACCESS_WIFI_STATE
BLUETOOTH
BLUETOOTH_ADMIN
BROADCAST_STICKY
CHANGE_NETWORK_STATE
CHANGE_WIFI_MULTICAST_STATE
CHANGE_WIFI_STATE
DISABLE_KEYGUARD
EXPAND_STATUS_BAR
FOREGROUND_SERVICE
GET_PACKAGE_SIZE
INSTALL_SHORTCUT
INTERNET
KILL_BACKGROUND_PROCESSES
MANAGE_OWN_CALLS
MODIFY_AUDIO_SETTINGS
NFC
READ_SYNC_SETTINGS
READ_SYNC_STATS
RECEIVE_BOOT_COMPLETED
REORDER_TASKS
REQUEST_COMPANION_RUN_IN_BACKGROUND
REQUEST_COMPANION_USE_DATA_IN_BACKGROUND
REQUEST_DELETE_PACKAGES
REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
SET_ALARM
SET_WALLPAPER
SET_WALLPAPER_HINTS
TRANSMIT_IR
USE_FINGERPRINT
VIBRATE
WAKE_LOCK
WRITE_SYNC_SETTINGS
"""

#PROTECTION_SIGNATURE
signature = """
BIND_ACCESSIBILITY_SERVICE
BIND_AUTOFILL_SERVICE
BIND_CARRIER_SERVICES
BIND_CHOOSER_TARGET_SERVICE
BIND_CONDITION_PROVIDER_SERVICE
BIND_DEVICE_ADMIN
BIND_DREAM_SERVICE
BIND_INCALL_SERVICE
BIND_INPUT_METHOD
BIND_MIDI_DEVICE_SERVICE
BIND_NFC_SERVICE
BIND_NOTIFICATION_LISTENER_SERVICE
BIND_PRINT_SERVICE
BIND_SCREENING_SERVICE
BIND_TELECOM_CONNECTION_SERVICE
BIND_TEXT_SERVICE
BIND_TV_INPUT
BIND_VISUAL_VOICEMAIL_SERVICE
BIND_VOICE_INTERACTION
BIND_VPN_SERVICE
BIND_VR_LISTENER_SERVICE
BIND_WALLPAPER
CLEAR_APP_CACHE
MANAGE_DOCUMENTS
READ_VOICEMAIL
REQUEST_INSTALL_PACKAGES
SYSTEM_ALERT_WINDOW
WRITE_SETTINGS
WRITE_VOICEMAIL
"""

#SPECIAL
special = """
ACCEPT_HANDOVER
ACCESS_CHECKIN_PROPERTIES
ACCESS_COARSE_LOCATION
ACCESS_FINE_LOCATION
ACCESS_LOCATION_EXTRA_COMMANDS
ACCESS_NETWORK_STATE
ACCESS_NOTIFICATION_POLICY
ACCESS_WIFI_STATE
ACCOUNT_MANAGER
ADD_VOICEMAIL
ANSWER_PHONE_CALLS
BATTERY_STATS
BIND_ACCESSIBILITY_SERVICE
BIND_APPWIDGET
BIND_AUTOFILL_SERVICE
BIND_CARRIER_MESSAGING_SERVICE
BIND_CARRIER_SERVICES
BIND_CHOOSER_TARGET_SERVICE
BIND_CONDITION_PROVIDER_SERVICE
BIND_DEVICE_ADMIN
BIND_DREAM_SERVICE
BIND_INCALL_SERVICE
BIND_INPUT_METHOD
BIND_MIDI_DEVICE_SERVICE
BIND_NFC_SERVICE
BIND_NOTIFICATION_LISTENER_SERVICE
BIND_PRINT_SERVICE
BIND_QUICK_SETTINGS_TILE
BIND_REMOTEVIEWS
BIND_SCREENING_SERVICE
BIND_TELECOM_CONNECTION_SERVICE
BIND_TEXT_SERVICE
BIND_TV_INPUT
BIND_VISUAL_VOICEMAIL_SERVICE
BIND_VOICE_INTERACTION
BIND_VPN_SERVICE
BIND_VR_LISTENER_SERVICE
BIND_WALLPAPER
BLUETOOTH
BLUETOOTH_ADMIN
BLUETOOTH_PRIVILEGED
BODY_SENSORS
BROADCAST_PACKAGE_REMOVED
BROADCAST_SMS
BROADCAST_STICKY
BROADCAST_WAP_PUSH
CALL_PHONE
CALL_PRIVILEGED
CAMERA
CAPTURE_AUDIO_OUTPUT
CAPTURE_SECURE_VIDEO_OUTPUT
CAPTURE_VIDEO_OUTPUT
CHANGE_COMPONENT_ENABLED_STATE
CHANGE_CONFIGURATION
CHANGE_NETWORK_STATE
CHANGE_WIFI_MULTICAST_STATE
CHANGE_WIFI_STATE
CLEAR_APP_CACHE
CONTROL_LOCATION_UPDATES
DELETE_CACHE_FILES
DELETE_PACKAGES
DIAGNOSTIC
DISABLE_KEYGUARD
DUMP
EXPAND_STATUS_BAR
FACTORY_TEST
FOREGROUND_SERVICE
GET_ACCOUNTS
GET_ACCOUNTS_PRIVILEGED
GET_PACKAGE_SIZE
GET_TASKS
GLOBAL_SEARCH
INSTALL_LOCATION_PROVIDER
INSTALL_PACKAGES
INSTALL_SHORTCUT
INSTANT_APP_FOREGROUND_SERVICE
INTERNET
KILL_BACKGROUND_PROCESSES
LOCATION_HARDWARE
MANAGE_DOCUMENTS
MANAGE_OWN_CALLS
MASTER_CLEAR
MEDIA_CONTENT_CONTROL
MODIFY_AUDIO_SETTINGS
MODIFY_PHONE_STATE
MOUNT_FORMAT_FILESYSTEMS
MOUNT_UNMOUNT_FILESYSTEMS
NFC
NFC_TRANSACTION_EVENT
PACKAGE_USAGE_STATS
PERSISTENT_ACTIVITY
PROCESS_OUTGOING_CALLS
READ_CALENDAR
READ_CALL_LOG
READ_CONTACTS
READ_EXTERNAL_STORAGE
READ_FRAME_BUFFER
READ_INPUT_STATE
READ_LOGS
READ_PHONE_NUMBERS
READ_PHONE_STATE
READ_SMS
READ_SYNC_SETTINGS
READ_SYNC_STATS
READ_VOICEMAIL
REBOOT
RECEIVE_BOOT_COMPLETED
RECEIVE_MMS
RECEIVE_SMS
RECEIVE_WAP_PUSH
RECORD_AUDIO
REORDER_TASKS
REQUEST_COMPANION_RUN_IN_BACKGROUND
REQUEST_COMPANION_USE_DATA_IN_BACKGROUND
REQUEST_DELETE_PACKAGES
REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
REQUEST_INSTALL_PACKAGES
RESTART_PACKAGES
SEND_RESPOND_VIA_MESSAGE
SEND_SMS
SET_ALARM
SET_ALWAYS_FINISH
SET_ANIMATION_SCALE
SET_DEBUG_APP
SET_PREFERRED_APPLICATIONS
SET_PROCESS_LIMIT
SET_TIME
SET_TIME_ZONE
SET_WALLPAPER
SET_WALLPAPER_HINTS
SIGNAL_PERSISTENT_PROCESSES
STATUS_BAR
SYSTEM_ALERT_WINDOW
TRANSMIT_IR
UNINSTALL_SHORTCUT
UPDATE_DEVICE_STATS
USE_BIOMETRIC
USE_FINGERPRINT
USE_SIP
VIBRATE
WAKE_LOCK
WRITE_APN_SETTINGS
WRITE_CALENDAR
WRITE_CALL_LOG
WRITE_CONTACTS
WRITE_EXTERNAL_STORAGE
WRITE_GSERVICES
WRITE_SECURE_SETTINGS
WRITE_SETTINGS
WRITE_SYNC_SETTINGS
WRITE_VOICEMAIL
"""

#DANGEROUS (list includes Permission groups and permissions)
dangerous = """
READ_CALENDAR
WRITE_CALENDAR
CALL_LOG	
READ_CALL_LOG
WRITE_CALL_LOG
PROCESS_OUTGOING_CALLS
CAMERA	
CAMERA
CONTACTS	
READ_CONTACTS
WRITE_CONTACTS
GET_ACCOUNTS
LOCATION	
ACCESS_FINE_LOCATION
ACCESS_COARSE_LOCATION
MICROPHONE	
RECORD_AUDIO
PHONE	
READ_PHONE_STATE
READ_PHONE_NUMBERS
CALL_PHONE
ANSWER_PHONE_CALLS
ADD_VOICEMAIL
USE_SIP
SENSORS	
BODY_SENSORS
SMS	
SEND_SMS
RECEIVE_SMS
READ_SMS
RECEIVE_WAP_PUSH
RECEIVE_MMS
STORAGE	
READ_EXTERNAL_STORAGE
WRITE_EXTERNAL_STORAGE
"""

permissions_vector = set()
for s in [normal, signature, special, dangerous]:
    permissions_vector.update(set(s.split()))

print("These are the permissions that we will test for:")
print(permissions_vector)

# 

# Let's make a function that can clean one permission (that is a string)

def clean_permission(p):
    # Android "built-in" permissions start with android.permission
    #  developers might have defined custom permissions with their own prefix, though
    #  This function removes the "android.permission." from the start of a string, if it is present
    prefix = "android.permission."
    if p.startswith(prefix):
        # This is "string slicing"
        #   str[start_index:end_index]
        return p[len(prefix):]
    else:
        # this is some custom permission - maybe that is interesting?
        return p



data = pd.read_csv("amd_data_permissions.csv")

print("\n\n--Column labels, i.e. values of x where data[x] is valid--")
print(data.columns)

print("\n\n--A data series / list of rows for column 'get_permissions'--")
print(data['get_permissions'])

print("\n\n--Permissions for first sample--")
print(data['get_permissions'][0])

################## The Pandas Way ######################################

#(note: we are outside the for loop now, this is an alertative to the stuff above:

print("\n\n------------------------------------------\n\n")

# Same idea, but we really just want to make a *new* column in our pd.DataFrame object that contains the final / cleaned version of the permissions column

# There are some extra rows somehow that have 'get_permissions' as the value :/ We should get rid of them:
# https://stackoverflow.com/questions/13851535/how-to-delete-rows-from-a-pandas-dataframe-based-on-a-conditional-expression
#   (otherwise the next step will throw an error)
data = data.drop(data[data.get_permissions == "get_permissions"].index )

# Call the ast.literal_eval function on each row. Pass the function row['get_permissions']
data['permissions_list'] = data.apply(lambda row: ast.literal_eval(row['get_permissions']) , axis=1)

# Okay, now permissions_list is a column in our DataFrame (we have one per row - each row is a malware sample)

print("\n\npermissions_list Column (before cleaning): ")
print(data['permissions_list'])


# Now let's clean it, like before!
data['permissions_list'] = data.apply(lambda row: [clean_permission(x) for x in row['permissions_list']] , axis=1)

# Woah, that one was kinda tricky!
#       row['permissions_list'] is our python list of permissions
#       [clean_permission(x) for x in row['permissions_list']] makes a NEW list, by cleaning each permission in the old list


# Okay, now permissions_list is a clean list of strings and a column in our DataFrame (we have one per row - each row is a malware sample)

print("\n\npermissions_list Column (after cleaning): ")
print(data['permissions_list'])


# Now let's split / featurize the list of strings based on some set of permissions that are interesting:
# https://datascience.stackexchange.com/questions/11797/split-a-list-of-values-into-columns-of-a-dataframe

# Note: we can do the same thing for any list (e.g. API calls), if we have a fixed set of possible values that we care about

# For each permission (p) that we care about...
for p in permissions_vector:
    # make a new column for that permission.
    # For each row, set the value of that column to the answer to: "Does the permissions_list column contain permission p?"
    data[p] = data.apply(lambda row: int(p in row['permissions_list']), axis=1 )


print("\n\n--New Column labels, i.e. values of x where data[x] is valid--")
print(data.columns)

# We can work with the data here, or re-export to a file

data.to_csv("amd_data.static.featurized.csv")

