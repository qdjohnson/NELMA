#!/usr/bin/env python
import sys
import androguard
from androguard import misc

import json

requested_features = [
#    'get_uses_implied_permission_list',
#    'get_requested_aosp_permissions_details',
    'get_permissions',
#    'get_declared_permissions_details',
#    'get_details_permissions',
]

def get_permissions(filename, requested_features):
    a, d, dx = androguard.misc.AnalyzeAPK(filename)

    features  = {f:"" for f in requested_features}
    features["file_path"] = filename

    for f in requested_features:
        try:
            features[f] = getattr(a, f)()
        except Exception as e:
            print("Error on Feature {}: {}".format(f,e))
    return features


features = get_permissions(sys.argv[1], requested_features)
for k,v in features.items():
    print("{}:{}".format(k,v))
    print("")

# Process the features and format as needed:

# Featurize the get_permissions output
for permission in features['get_permissions']:
    features[permission]=True

del features['get_permissions']
 
with open('features.json', 'w') as f:
    f.write(json.dumps(features))

