#!/usr/bin/env python

import androguard
from androguard import misc

import csv
import os
import types

import asn1crypto

#from androguard import session

# get a default session
#sess = misc.get_default_session()


def get_metadata_features(a):
    features = {
        "activities":a.get_activities(),
        "permissions":a.get_permissions(),
        "package":a.get_package(),
        "app_name":a.get_app_name(),
        "app_icon":a.get_app_icon(),
        "androidversion_code":a.get_androidversion_code(),
        "androidversion_name":a.get_androidversion_name(),
        "min_sdk_version":a.get_min_sdk_version(),
        "max_sdk_version":a.get_max_sdk_version(),
        "effective_target_sdk_version":a.get_effective_target_sdk_version(),
        "main_activity":a.get_main_activity(),
    }
    return features

#def get_source_code(dx):
#    print("Classes: {}".format(dx.get_classes_names())) 
#    source = {c:dx.get_class(c) for c in dx.get_class_names()}
#    return source

#def get_java_features(dx):
#    features = {
#        "classes_names":dx.get_classes_names(),
#        "source_code":get_source_code(dx)
#    }


def get_java_features(dx):
    classes=dx.get_classes()
    features = {
        "vm_classes":[c.get_vm_class for c in classes]
    }
    return features


def printd(d):
    for k,v in d.iteritems():
        print("{}:{}".format( k, v))


def get_obj_methods(d, obj):
    global requested_features
    res = {feature:getattr(obj,feature) for feature in dir(obj) if callable(getattr(obj, feature)) and feature in requested_features }
    if d is not None:
        d.update(res)
    else:
        return res

def resolve_features(features): 
    for f in features.copy():
        try:
            features[f]=features[f]()
            #print("{} is callable".format(f))
            if isinstance(features[f], types.GeneratorType):
                print("Collapsing generator...")
                features[f] = list(features[f])

            if type(features[f]) is list and isinstance (features[f][0], androguard.core.analysis.analysis.StringAnalysis):
                print("Collapsing StringAnalysis List")
                res = []
                for s in features[f]:
                    res.append(s.get_value())
                features[f] = res
            elif isinstance (features[f], androguard.core.analysis.analysis.StringAnalysis):
                print("Collapsing StringAnalysis")
                features[f] = features[f].get_value()
            elif type(features[f]) is list and isinstance(features[f][0], asn1crypto.x509.Certificate):
                print("Collapsing Certificate List")
                res = []
                for s in features[f]:
                    res.append(s.human_friendly)
                features[f] = res
            #elif isinstance(features[f], asn1crypto.x509.Certificate):
            #    print("Collapsing Certificate")
            #    features[f] = features[f].human_friendly
            if f == "get_android_manifest_axml":
                features[f] = features[f].get_xml().replace('\r', '').replace('\n', '')
            elif f == "get_android_manifest_xml":
                del features[f]
         

        except Exception as e:
            print("Exception on feature {}: {}".format(f,e))
            del features[f]
            pass

def get_class_features(dx):
    class_features = {c:get_obj_methods(None, c) for c in dx.get_classes()}
    for d in class_features.values():
        resolve_features(d)
    return class_features


def dump_static_features(filename):
    global all_features
    global requested_features

    a, d, dx = misc.AnalyzeAPK(filename)

    features  = {f:"" for f in requested_features}
    features["file_path"] = filename

    #get_obj_methods(features, a)
    #get_obj_methods(features, d)
    #get_obj_methods(features, dx)

    for f in requested_features:
        try:
            features[f] = getattr(a, f)()
        except Exception as e:
            print("Error on Feature {}: {}".format(f,e))
               
    # "Custom" features:
    #features["class_features"] = get_class_features(dx)
    #features["java_features"] = get_java_features(dx)

    #resolve_features(features)

    all_features.update({os.path.basename(filename):features})



def write_csv(all_features, header):
    global fields
    #fields = set()
    for k,v in all_features.items():
        #print("{}:{}".format(k,v))
        fields = fields.union(v.keys())
    fields_list = ["sample_id"] + list(fields)


    #print("fields: {}".format(fields))
    #print("all_features: {}".format(all_features))

    print("-----------FLUSIHING TO DISK--------------")

    with open("benign.static.csv", "ab") as f:
        #w = csv.DictWriter(f, all_features.keys())
        #w = csv.DictWriter(f, next(all_features.itervalues()).keys())
        w = csv.DictWriter(f, fields_list)
        if header:
            w.writeheader()
        for r in all_features.keys():
            row = {"sample_id":r}
            row.update(all_features[r])
            for k,v in row.items():
                try:
                    str(v)
                except Exception:
                    row[k] = "CANNOT_BE_DISPLAYED"

            w.writerow(row) 
        #w.writerows(all_features)
        #for k,v in all_features.items():
        #    w.writerow([key, value])


global all_features
global fields
global requested_features

with open("permissions.features", 'r') as f:
    requested_features = f.read().splitlines()


fields = set()

all_features = {} 

topdir = '/home/testbed/Data/benign/'
 
# The arg argument for walk, and subsequently ext for step
exten = '.apk'
 
def step(ext, dirname, names):
    global all_features
    ext = ext.lower()

    cap = 2
    count = 0
    header = True
    for name in names:
        if name.lower().endswith(ext):
            filename = os.path.join(dirname, name)
            print("Analyzing({}): {}".format(count, filename))
            try:
                dump_static_features(filename)
                count = count + 1
                if count >= cap:
                    count = 0
                    print("-----------FLUSIHING TO DISK (ATTEMPT)--------------")
                    write_csv(all_features, header=header)
                    all_features={}
                    header=False
            except Exception as e:
                print("Error:{}".format(e)) 

# Start the walk
os.path.walk(topdir, step, exten)


#import json

#with open("amd_data.static.json", "w") as f:
#    f.write(json.dumps(all_features, encoding='latin1'))

write_csv(all_features, False)

#write_csv({"sample1":{"f1":"v1", "f2":"v2"}, "sample2":{"f1":"v1", "f2":"v2", "f3":"v3"} })


# Save the session to disk
#session.Save(sess, "androguard_session.p")

# Load it again
#sess = session.Load("androguard_session.p")
