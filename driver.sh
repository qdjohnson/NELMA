#!/bin/bash
# Purge pyc
find . -type f -name "*.pyc" -exec rm {} \;
./ingest/reset-all.sh
./ingest/bulk-ingest.sh
./utils/exec_all.py predictors/
./utils/exec_all.py knowledge_integrators/ 
#) 2>&1 | tee experiment.log

