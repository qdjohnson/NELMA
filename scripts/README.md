# NELMA Scripts

This directory contains admin scripts intended to be run interactively

## delete_models

A script that deletes all underlying Predictor and Knowledge Integrator Models

### Purpose

This script resets all models to a (mostly) blank slate and forces them to retrain on the currently available (i.e. not hidden) data.
By strategically hiding, unhiding, and deleting models, it is possible to force NELMA to train on the samples of your choosing.


### Usage
```
./scripts/delete_models.py
```
