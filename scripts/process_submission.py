#!/usr/bin/env python

from config import CONFIG

import driver_utils
import extractor_utils
import extractors
import sys, os

def main():
    CONFIG['LOGGERS']['DRIVER'].info('Manually extracting: {}'.format(sys.argv[1:]))
    extractor_utils.driver(extractors.__all__, sys.argv[1:])
    CONFIG['LOGGERS']['DRIVER'].info('Manually classifying predicitions: {}'.format(sys.argv[1:]))
    driver_utils.exec_all([driver_utils.exec_all([CONFIG['PREDICTORS_DIR'], CONFIG['KI_DIR']])], skip_init=True) 

if __name__=="__main__":
    main()
