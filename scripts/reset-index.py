#!/usr/bin/env python

from config import CONFIG
import db_utils


import pandas as pd

def reset_predictions():
    r = db_utils.delete_index(db_utils.get_prediction_index())
    print(r.text)
    df = db_utils.get_all_samples()
    print("Adding prediciton placeholders for {} samples...".format(len(df.index)))
    for i, row in df.iterrows():
        meta = db_utils.clean_meta(row)
        meta['nelma_label'] = None
        meta['sample_weight'] = 1.0
        #print("Adding prediciton placeholder for {}".format(meta))
        db_utils.add_prediction_placeholder(meta)

def hide_all():
    db_utils.hide_all('_all')

def unhide_all():
    db_utils.unhide_all('_all')

if __name__ == "__main__":
    reset_predictions()
