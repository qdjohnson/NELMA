#!/usr/bin/env python

# Extract features using Androguard
#  (perform static analysis on an APK at rest)

# Note: each script in extractors/ can function as *multiple* logical feature extractors
# A feature extractor should define a main() method that takes in a sample (filepath)
# it should return a dictionary in this form:
#  {
#    "feature_set_id_1":{...},
#    "feature_set_id_2":{...}, ...
#  }
# The outermost keys of the dictionary will be used as an Elasticsearch index (db table) names and should uniquely identify
# the feature set that this extractor is producing.
# The inner dictionaries contain the actual features belonging to that set

# This is permitted since many libraries (like Androguard) make incur a high cost to parse / analyze the application (reading it into memory, etc.).
# If possible, in these cases, all "related" feature sets that can be computed at low additional cost should be computed in a single extractor script.

# However, if there is there is little or no work reuse during the computation of different feature sets (e.g. you have to read in the file twice),
#  then do NOT put mutiple logical feature extractors in the same script, as they will no longer be automatically parallelized for you.

# Also note that these multiple feature sets can be combined. A predictor is allowed to query multiple feature sets, but if this is common,
#  you should consider combining small feature or ineffective feature sets to make a larger one

from config import CONFIG
import extractor_utils
import db_utils

import androguard
from androguard import misc

import os

from urlparse import urlparse
import tld

# The main method with this signature is required and the only one that matters (it will be called by utils/extract_features.py
#  The rest are helper methods to improve readability.
def main(sample):
    # Do initial analysis, but then reuse the work for multiple feature sets
    # Useful resources:
    #   https://media.readthedocs.org/pdf/androguard/latest/androguard.pdf
    #   http://www.technotalkative.com/part-1-reverse-engineering-using-androguard/
    #   https://forensics.spreitzenbarth.de/2015/10/05/androguard-a-simple-step-by-step-guide/
    a, d, dx = androguard.misc.AnalyzeAPK(sample)

    # This is just a little trick to maximize DRY (Don't Repeat Yourself) since every function call is very similar
    #  It also allows us to instrument our metods with safe_call.
    #  We use safe call because 1 feature set failing should not be fatal to the other feature sets
    # (the only thing that matters is the final result; this is not necessary for the feature extractor to work)
    prefix = "static-"
    funcs = {
        'permissions':lambda: get_permissions(a),
        'services':lambda: get_services(a),
        'receivers':lambda: get_receivers(a),
        'imports':lambda: get_imports(dx),
        'strings':lambda: get_strings(d),
        'hardware-features':lambda: get_hardware_features(a),
    }
    # Return a dicionary such that the outermost keys are index names (e.g. static-permissions)
    #   and the values are dictionaries returned by the get_XXXX helper methods 
    features =  {"{}{}".format(prefix, name):extractor_utils.safe_call(func, "{}.get_{}".format(os.path.basename(__file__), name)) for name, func in funcs.items() }

    CONFIG['LOGGERS']['EXTRACT'].debug("\n\n\n")
    for _id, f in features.items():
        CONFIG['LOGGERS']['EXTRACT'].info("Feature Set {} contains {} features - {}".format(_id, len(f.keys()), f.keys()[:3]))
    CONFIG['LOGGERS']['EXTRACT'].debug("\n\n\n")

    for k, v in features.items():
        CONFIG['LOGGERS']['EXTRACT'].debug("{}:{}".format(k, v.keys()[:10]))
    CONFIG['LOGGERS']['EXTRACT'].debug("\n\n\n")

    return features

# Note: these functions are explicitly defined so it is easy to add cleaning / formatting code for each feature set
#   (If you want to dynamically call many functions, you could do: getattr(a, 'my_func')()
def get_permissions(a):
    permissions = a.get_permissions()
    # Featurize the list of permissions
    return extractor_utils.featurize_bool(permissions)

def get_services(a):
    services = a.get_services()
    # Featurize the list of permissions
    return extractor_utils.featurize_bool(services)

def get_receivers(a):
    receivers = a.get_receivers()
    # Featurize the list of permissions
    return extractor_utils.featurize_bool(receivers)

#def get_classes(d):
#    classes = []
#    #num_minified_classes = 0
#    for obj in d:
#        # get_classes_names() can be used, but lists internal classes that are likely unique to the sample
#        classes.extend([c.name for c in obj.get_external_classes()])
#        #for c in obj.get_external_classes():
#            ## Lots of class names are minified, quickly exceeding the database's limit on number of fields
#            #if len(c) > 5:
#            #    classes.append(c)
#            #else:
#            #    num_minified_classes += 1
#    features = extractor_utils.featurize_bool(classes)
#    #features["num_minified_classes"] = num_minified_classes
#    return features

def get_imports(dx):
    min_len = 5
    depth = 3
    imports = [c.name for c in dx.get_external_classes()]
    #CONFIG['LOGGERS']['EXTRACT'].debug(imports[:3])
    # There are a LOT of possible imports (tens of thousands)
    #  reduce the total number of possible columns by simplifying a bit
    # Turn something like Landroid/net/wifi/WifiManager$WifiLock;: into android/net/wifi
    imports = ['/'.join( name.split('/')[:depth-1] ) for name in imports ]
    #CONFIG['LOGGERS']['EXTRACT'].debug(imports[:3])
    # Clean up garbage around library names
    cleaned = []
    num_minified = 0
    for name in imports:
        if name.startswith('['):
            name = name[1:]
        if name.startswith('L'):
            name = name[1:]

        if name.endswith(']'):
            name = name[:-1]
        if name.endswith(':'):
            name = name[:-1]
        if name.endswith(';'):
            name = name[:-1]
    
        # assert should be safe
        name = name.rsplit("$", 1)[0]

        # Don't track minified classes separately, group them together
        #   assume all library names shorter than min_len have been minified
        if sum([len(n) for n in name.split('/')]) >= min_len:
            cleaned.append(name)
        else:
            num_minified += 1
    
    imports = cleaned 
    CONFIG['LOGGERS']['EXTRACT'].debug(imports[:3])
    features = extractor_utils.featurize_freq(imports)

    features['num_minified'] = num_minified
    return features

# Do a little strings analysis: extract URLs and Email addresses
def get_strings(d):
    urls = []
    emails = []
    for obj in d:
        # Regex modified from https://stackoverflow.com/questions/6883049/regex-to-extract-urls-from-href-attribute-in-html-with-python
        urls.extend( obj.get_regex_strings('\w+?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+') )
        # http://www.ex-parrot.com/~pdw/Mail-RFC822-Address.html ... this is a simplified version
        #   https://www.regular-expressions.info/email.html

        # TODO: Verify this works
        emails.extend( obj.get_regex_strings('\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b') )

    netlocs = {}
    tlds = {}
    schemes = {}
    endpoints = {}
    ports = {}
    
    tld_errors = [] 
    for u in urls:
        try:
            o = urlparse(u)
            extractor_utils.inc_freq(netlocs, o.netloc)
            
            try:
                extractor_utils.inc_freq(tlds, tld.get_tld(u))
            except Exception as e:
                tld_errors.append(o.netloc)
            extractor_utils.inc_freq(schemes, o.scheme)
            extractor_utils.inc_freq(endpoints, os.path.basename(o.path) )
 
            if o.netloc:
                split_loc = o.netloc.rsplit(':', 1)
                port = None
                if len(split_loc) > 1:
                   port= split_loc[1] 
                if not port:
                    if o.scheme == "http":
                        port = '80'
                    elif o.scheme == "https":
                        port = '443'
                if port:
                    extractor_utils.inc_freq(ports, port)

        except Exception as e:
        # Ignore edge cases, this is best effort
            CONFIG['LOGGERS']['EXTRACT'].error(e)
            pass
        
    CONFIG['LOGGERS']['EXTRACT'].warning("Could not extract TLD from {} URLS".format(len(tld_errors)))
    CONFIG['LOGGERS']['EXTRACT'].debug("Could not extract TLD from: {}".format(tld_errors))
    email_domains = {}
    for e in emails:
        split_email = e.split('@',1)
        if len(split_email) > 1:
            extractor_utils.inc_freq(email_domains, split_email[1])

    for d in (netlocs, schemes, endpoints, ports, email_domains, tlds):
        try:
            del d['']
        except Exception as e:
            pass

    features = {}
    features['netlocs'] = netlocs
    features['tlds'] = tlds
    CONFIG['LOGGERS']['EXTRACT'].debug(tlds)
    features['schemes'] = schemes
    CONFIG['LOGGERS']['EXTRACT'].debug(schemes)
    #features['endpoints'] = endpoints

    # very little repetition in URL endpoints
    features['raw_url_endpoints'] = endpoints.keys()
    
    CONFIG['LOGGERS']['EXTRACT'].debug(endpoints)
    features['ports'] = ports
    CONFIG['LOGGERS']['EXTRACT'].debug(ports)
    features['email_domains'] = email_domains

    features["raw_urls"] = urls
    features["raw_emails"] = emails

    #features.update(extractor_utils.featurize_freq(urls))
    #features.update(extractor_utils.featurize_freq(emails))

    return features

def get_hardware_features(a):
    features = a.get_features()
    return extractor_utils.featurize_bool(features)

if __name__ == "__main__":
    extractor_utils.debug_extractor_driver(os.path.splitext(os.path.basename(__file__))[0] )
    #extractor_utils.single_extractor_driver(os.path.splitext(os.path.basename(__file__))[0] )
