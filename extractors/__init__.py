#!/usr/bin/env python

# When someone imports extractors, give them all extractors in this directory
# (allow extractors to be dynamically added just by dropping them in the right directory)

# See https://stackoverflow.com/questions/1057431/how-to-load-all-modules-in-a-folder

import os
import glob
modules = glob.glob(os.path.dirname(__file__)+"/*.py")
__all__ = [ os.path.basename(f)[:-3] for f in modules if os.path.isfile(f) and os.path.basename(f) not in ['config.py', '__init__.py'] ]

from . import *
