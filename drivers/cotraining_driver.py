#!/usr/bin/env python

# Co-training driver
#  Simulate starting from a small subset of labeled examples
#  Use NELMA to generate training labels iteratively

# Get all available samples (store them locally)
#   Select a random subset to use as initial labeled set
#   Do K-fold cross validation on the rest of the data
#    - for each k, hold out 1 fold as the test set
#    -   Parition the training set into NUM_STEPS partitions 
#    -   for each step, reveal the coresponding parition to NELMA and score each model against the held-out fold
#    -   after all k-1 folds have been used to predict, delete all models and nelma_labels and use the next fold k
#   Repeat this process with different random initial subsets

# Import NELMA utils
from config import CONFIG
import db_utils
import driver_utils

import pandas as pd
import numpy as np

import sklearn
from sklearn.model_selection import train_test_split, StratifiedKFold
#from sklearn.metrics import confusion_matrix, accuracy_score, balanced_accuracy_score, f1_score, precision_score, recall_score

import time
import os

NELMA_KI='nelma-ki-mlp-deep'
START_TIME = time.time()
NUM_SPLITS=3
TRAIN_SIZE=0.70

NUM_STEPS=4

TRIALS=3

# Partition df into train_set, test_set
# train_set contains samples (drawn without replacement) from df with approximately equal class distributions
# test_set contains the remaining examples 
# if sample_size is less than 1, it is treated as a fraction of df's rows
# else, it is the desired approx. number of samples in train_set
def get_initial_split(df, sample_size=0.02, class_balance=0.7):
    malware = df [ df['ground_truth'] ]
    benign = df [ ~df['ground_truth'] ]
    if sample_size < 1:
        sample_size = int(len(df.index) * sample_size)
    else:
        sample_size = int(sample_size)
    malware_size = max( int(sample_size * class_balance) , 1)
    benign_size = max( int(sample_size * (1 - class_balance)), 1)
    CONFIG['LOGGERS']['DRIVER'].info("Selecting {} positives and {} negatives...".format(malware_size, benign_size))
    selected = pd.concat([malware.sample(malware_size) , benign.sample(benign_size)]).reset_index()
    not_selected = df[ ~df['sample_id'].isin(list(selected['sample_id'])) ]
    selected.head()
    not_selected.head()
    CONFIG['LOGGERS']['DRIVER'].info("Selected {} out of {}, leaving {}".format(len(selected.index), len(df.index), len(not_selected.index)))
    return selected, not_selected

def get_split(df, size=0.50):
    return sklearn.model_selection.train_test_split(df, train_size=size, stratify=df['ground_truth'])

def score_predictors(train_df, test_df, details):
    CONFIG['LOGGERS']['DRIVER'].info("Scoring against Train:\n{}\nTest:\n{}".format(train_df['ground_truth'].value_counts(), test_df['ground_truth'].value_counts() ))
    _score_predictors(test_df, details, is_test=True)
    _score_predictors(train_df, details, is_test=False)

def _score_predictors(score_df, details, is_test=True, prefix=''):
    timestamp = time.time()
    df = db_utils.get_all_predictions()
    ki_list = db_utils.get_all_ki()
    CONFIG['LOGGERS']['DRIVER'].info("Test Set? {} - contains {} elements".format(is_test, len(df.index)))

    # Throw out all the data not in the test set
    df = df[ df['sample_id'].isin(list(score_df['sample_id']))]
    CONFIG['LOGGERS']['DRIVER'].info("Scoring against:\n{}\n({} rows total)".format(df.head(), len(df.index)))

    CONFIG['LOGGERS']['DRIVER'].info("\n{}\nTrial {} - Split {} - Step {}\n{}".format('-'*80, details['trial'], details['split'], details['step'], '-'*80))

    predictions = [c for c in list(df.columns) if c.endswith(".prediction") and len(c.split('.')) == 2]
    CONFIG['LOGGERS']['DRIVER'].info("Predictors: {}".format(predictions))
    for col in predictions:
        predictor_id = col.split('.')[0]
        y_true = df['ground_truth']
        y_pred = df[col]
        meta = {
            'timestamp':timestamp,
            'predictor_id':predictor_id,
            'is_test':is_test,
            'is_ki':predictor_id in ki_list
        }
        # Decorate our performance report with everything in details
        meta.update(details)

        metrics = driver_utils.get_model_metrics(y_true.astype(bool), y_pred.astype(bool), meta, prefix)
        for k, v in metrics.items():
            CONFIG['LOGGERS']['DRIVER'].info("{}:{}".format(k,v))
        db_utils.upload_performance_metrics(metrics)

# Since there will be probably be class imbalance, we want to make sure our initial sample is "fair"
# Inspired by https://stackoverflow.com/questions/45839316/pandas-balancing-data
#def balance_classes(samples):
#    # Split the dataset into malicious and benign
#    dfs = [  df[df['ground_truth']] , df[~df['ground_truth']] ]
#    sizes = [ len(df.index) for df in dfs ]
#    majority_size = max(sizes)
#    res = pd.DataFrame()
#    for df, size in zip(dfs, sizes):
#        if size < majority_size:
#            res = pd.concat(res, df.sample(majority_size, replace=True))
#        else:
#            res = pd.concat(res, df)
#
#    return res
 
# Since there will be probably be class imbalance, we want to make sure our initial sample is "fair"
# However, each predictor queries the database directly, and we don't want to add duplicate entries to the database
# Solution: increase the sample_weights of the initial minority class and decrease the weights of the initial majority class
# (the amount to adjust these weights are hyperparameters and should be tuned)
# (whether the balancing should only be done on the initial sample or repeated for each sample batch should also be determined through experiementation)
# (Since NELMA's KI has the power to reweight samples, perhaps non-initial weighting should be done by it)
def balance_classes(samples):
    # Split the dataset into malicious and benign
    malware = samples[ samples['ground_truth'] ]
    benign = samples[ ~samples['ground_truth'] ] 
    sizes = [ len(df.index) for df in [malware, benign] ]
    majority_size = max(sizes)
    minority_size = min(sizes)

    # Scale majority "down" by factor, scale minority "up" by factor
    factor = (majority_size / float(minority_size)) # / 2.0

    for df in [malware, benign]:
        if len(df.index) == minority_size:
            CONFIG['LOGGERS']['DRIVER'].info("Increasing minority weights by x{}".format(factor))
            for sample_id in df['sample_id']:
                # Original labeled samples should be given much higher weights than our generated labels
                # Minority class weights are increased and majority class weights are reduced
                #   on the initial split (if NELMA predicts homogenously, we likely can't recover on our own)
                db_utils.update_sample_weight(sample_id, 20.0*factor, '_all')
        else:
            CONFIG['LOGGERS']['DRIVER'].info("Decreasing majortiy weights by x{}".format(factor))
            for sample_id in df['sample_id']:
                db_utils.update_sample_weight(sample_id, 20.0/factor, '_all')
     
   
def run_trial(trial, labeled, unlabeled):    
    trial_start = time.time()
    driver_utils.delete_models()
    CONFIG['LOGGERS']['DRIVER'].info("{} initial, labeled samples".format(len(labeled.index)))
    CONFIG['LOGGERS']['DRIVER'].info(labeled['ground_truth'].value_counts())
    CONFIG['LOGGERS']['DRIVER'].info("{} initial, unlabeled samples".format(len(unlabeled.index)))
    CONFIG['LOGGERS']['DRIVER'].info(unlabeled['ground_truth'].value_counts())

    # Balance classes (change intial sample weights to prevent minority class from being ignored)
    balance_classes(labeled)
    #db_utils.increment_labeled_count(len(labeled.index))

    num_labeled = len(labeled.index)

    #skf = sklearn.model_selection.StratifiedKFold(n_splits=K, shuffle=True)
    
    for split in range(NUM_SPLITS):
        split_start = time.time()
        details = {
            "trial":trial,
            "step":0,
            "split":split,
        }
        # Rehide the set we are about to split
        db_utils.hide(unlabeled, '_all')

        # Reset the number of labeled samples we have seen
        # TODO: Ensure all predictors are created
        db_utils.update_predictor(None, 'num_labeled', num_labeled)
        # Also reset num_seen 
        db_utils.update_predictor(None, 'num_seen', num_labeled)

        details['num_labeled'] = num_labeled
        details['num_seen'] = num_labeled

        #for train_index, test_index in skf.split(unlabeled, unlabeled['ground_truth']):
        train_set, test_set = get_split(unlabeled, size=TRAIN_SIZE)

        CONFIG['LOGGERS']['DRIVER'].info("Building initial models...")
        # Make sure evaluation mode is off (in case we crashed)
        db_utils.set_eval_mode(False)
        # Run all predcitors, then all knowledge integrators for evaluation
        driver_utils.exec_all([CONFIG['PREDICTORS_DIR'], CONFIG['KI_DIR']])

        CONFIG['LOGGERS']['DRIVER'].info("Scoring initial models (trained on initial labeled)...")
        #score_predictors(train_set, test_set, details)

        seen_examples = labeled

        score_predictors(labeled, test_set, details)


        # Iterate through the training data (which we treat as 'unlabeleld') in NUM_STEPS chunks
        for step, sample_batch in enumerate(np.array_split(train_set, NUM_STEPS)):
            details['step'] = step + 1
            # Add a few ground truth samples at every step
            # By overriding the current value of nelma_label to ground_truth, the training_label will be set to ground_truth when predictors retrain
            # (This "exposes" the ground_truth to all predictors and KIs)
            new_labeled, new_train_set = get_initial_split(sample_batch, sample_size=0.05)
            balance_classes(new_labeled)
            CONFIG['LOGGERS']['DRIVER'].info("Adding {} labeled samples with classes\n{}".format(len(new_labeled.index), new_labeled['ground_truth'].value_counts() ))
            #db_utils.remove_nelma_labels(new_labels)
            db_utils.override_nelma_labels(new_labeled, new_labeled['ground_truth'])

            num_labeled += len(new_labeled.index)
            #db_utils.increment_labeled_count(len(new_labeled.index))
            db_utils.update_predictor(None, 'num_labeled', num_labeled)

            # Expose the samples to the predictors
            # (Note: if nemlma_label is present, it will "mask" ground_truth)
            db_utils.unhide(sample_batch, '_all')

            # Update details so we can graph performance by these fields
            details['num_labeled'] = num_labeled
            details['num_seen'] += len(sample_batch.index)
            # Set all predictors in evaluation_mode (disable retraining)
            #db_utils.set_eval_mode(True)

            # Run all predcitors, then all knowledge integrators for evaluation
            driver_utils.exec_all([CONFIG['PREDICTORS_DIR'], CONFIG['KI_DIR']])

            seen_examples = pd.concat([seen_examples, sample_batch], axis=0, ignore_index=True, sort=False)

            score_predictors(seen_examples, test_set, details)
        
            # Disable evaluation_mode (enable retraining)
            db_utils.set_eval_mode(False)

            # Rerun NEMLA, allowing all predictors and Knowledge integrators to retrain on the new data
            #driver_utils.exec_all([CONFIG['PREDICTORS_DIR'], CONFIG['KI_DIR']])
            #split = split + 1

        # To prevent data from bleeding over between splits, delete all the models
        # (Note: samples in "labeled" are unhidden, so each model has at least that to train on)
        CONFIG['LOGGERS']['DRIVER'].info("Cleaning up after split {}...".format(split))
        driver_utils.delete_models()

        t = time.time() - split_start
        #https://stackoverflow.com/questions/3620943/measuring-elapsed-time-with-the-time-module
        t = time.strftime( "%H:%M:%S", time.gmtime(t) )
        CONFIG['LOGGERS']['DRIVER'].info("Elapsed time (Split {}): {}".format(split, t))

    t = time.time() - trial_start
    #https://stackoverflow.com/questions/3620943/measuring-elapsed-time-with-the-time-module
    t = time.strftime( "%H:%M:%S", time.gmtime(t) )
    CONFIG['LOGGERS']['DRIVER'].info("Elapsed time (Trial {}): {}".format(trial, t))

# TODO: write python utilities for this
def prepare_db(labeled):
    # Set up the database for this trial

    CONFIG['LOGGERS']['DRIVER'].info("Removing all nelma_labels...")
    db_utils.remove_nelma_labels()

    os.system("./scripts/reset-index.py predictions")
    

    db_utils.hide_all('_all')
    db_utils.unhide(labeled, '_all')
    CONFIG['LOGGERS']['DRIVER'].info("Resetting all sample_weights...")
    db_utils.update_sample_weight(None, 1.0, '_all')


def reset_performance():
    os.system("./ingest/reset-index.sh performance")

def main():
    CONFIG['LOGGERS']['DRIVER'].info("Running Cotraining driver with settings:\nNELMA_KI:{}\nNUM_SPLITS:{}\nNUM_STEPS:{}\nTRIALS:{}\n".format(NELMA_KI, NUM_SPLITS, NUM_STEPS, TRIALS))
    df = db_utils.set_primary_ki(NELMA_KI)
    df = db_utils.get_all_samples()
    CONFIG['LOGGERS']['DRIVER'].info("{} samples available".format(len(df.index)))

    # Reset performance data for this experiment
    reset_performance()

    # Since the initial labeled samples are random, repeat with multiple trials
    for trial in range(1, TRIALS+1):
    
        CONFIG['LOGGERS']['DRIVER'].info("Running Trial {}".format(trial))
        labeled, unlabeled = get_initial_split(df)

        prepare_db(labeled)

        CONFIG['LOGGERS']['DRIVER'].info("\n\n{}\nLabeled Data:\n{}".format('-'*80, labeled['ground_truth'].value_counts()))
        CONFIG['LOGGERS']['DRIVER'].info("\nUnlabeled Data:\n{}".format(unlabeled['ground_truth'].value_counts()))

        # Execute the trial
        run_trial(trial, labeled, unlabeled)

    t = time.time() - START_TIME
    #https://stackoverflow.com/questions/3620943/measuring-elapsed-time-with-the-time-module
    t = time.strftime( "%H:%M:%S", time.gmtime(t) )
    CONFIG['LOGGERS']['DRIVER'].info("Elapsed time (Total): {}".format(t))

if __name__ == "__main__":
    main()
